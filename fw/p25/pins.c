#include <avr/io.h>

#include <reving/reving.h>
#include "pins.h"


void pins_init(void) {
	// p1..p6 (0..5)
	DDRD &= ~0xFC;
	PORTD |= 0xFC;
	// p7..p14 (6..13)
	DDRC = 0x00;
	PORTC = 0xFF;
	// p15..p22 (14..21)
	DDRA = 0x00;
	PORTA = 0xFF;
	// p23..p25 (22..24)
	DDRB &= ~0x07;
	PORTB |= 0x07;
}

uint8_t reving_get_num_pins(void) {
	return 25;
}

#define OUT(ddr, port, pmask, mode) \
	switch (mode) { \
		case REVING_PIN_0: \
			port &= ~pmask; \
		case REVING_PIN_1: \
			ddr |= pmask; \
			break; \
		case REVING_PIN_IN: \
			ddr &= ~pmask; \
			port |= pmask; \
			break; \
	}

void reving_cmd_output(uint8_t pin, reving_pin_mode_t mode) {
	uint8_t pmask;
	if (pin < 6) {
		pmask = _BV(pin + 2);
		OUT(DDRD, PORTD, pmask, mode);
	} else if (pin < 14) {
		pmask = _BV(pin - 6);
		OUT(DDRC, PORTC, pmask, mode);
	} else if (pin < 22) {
		pmask = _BV(pin - 14);
		OUT(DDRA, PORTA, pmask, mode);
	} else {
		pmask = _BV(pin - 22);
		OUT(DDRB, PORTB, pmask, mode);
	}
}

void reving_cmd_read(uint8_t *pin_data) {
	pin_data[0] = (PIND >> 2) | ((PINC & 1) << 6);
	pin_data[1] = (PINC >> 1);
	pin_data[2] = (PINA & 0x7F);
	pin_data[3] = (PINA >> 7) | ((PINB & 7) << 1);
}
