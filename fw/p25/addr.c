#include <avr/eeprom.h>

#include <reving/addr.h>


static uint8_t EEMEM addr;

uint8_t reving_get_address(void) {
	return eeprom_read_byte(&addr);
}

void reving_set_address(uint8_t new_addr) {
	eeprom_update_byte(&addr, new_addr);
}
