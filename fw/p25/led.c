#include <avr/io.h>

#include "led.h"


void led_init(void) {
	DDRB |= _BV(PB3);
	PORTB |= _BV(PB3);
}

void led_toggle(void) {
	PORTB ^= _BV(PB3);
}
