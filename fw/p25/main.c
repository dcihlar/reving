#include <reving/reving.h>
#include "uart.h"
#include "pins.h"
#include "led.h"


int main() {
	pins_init();
	uart_init();
	led_init();

	for (;;) {
		reving_task();
	}

	return 0;
}
