#include <avr/io.h>

#include <reving/reving.h>
#include "pins.h"


void pins_init(void) {
	// p1..p6 (0..5)
	DDRB &= ~0x3F;
	PORTB |= 0x3F;
	// p7..p11 (6..10)
	DDRD &= ~0x7C;
	PORTD |= 0x7C;
}

uint8_t reving_get_num_pins(void) {
	return 11;
}

#define OUT(ddr, port, pmask, mode) \
	switch (mode) { \
		case REVING_PIN_0: \
			port &= ~pmask; \
		case REVING_PIN_1: \
			ddr |= pmask; \
			break; \
		case REVING_PIN_IN: \
			ddr &= ~pmask; \
			port |= pmask; \
			break; \
	}

void reving_cmd_output(uint8_t pin, reving_pin_mode_t mode) {
	uint8_t pmask;
	if (pin < 6) {
		pmask = _BV(pin);
		OUT(DDRB, PORTB, pmask, mode);
	} else {
		pmask = _BV(pin - 6 + 2);
		OUT(DDRD, PORTD, pmask, mode);
	}
}

void reving_cmd_read(uint8_t *pin_data) {
	pin_data[0] = (PINB & 0x3F) | ((PIND << 4) & 0x40);
	pin_data[1] = (PIND >> 3) & 0x0F;
}
