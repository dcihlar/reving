#ifndef _OSCCAL_H_
#define _OSCCAL_H_

// Note: the implementation is in addr.c
//       so both programs can preserve the same EEPROM variables locations

void osc_calibrate(void);
void osc_calibration_save(void);

#endif
