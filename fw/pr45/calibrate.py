#!/usr/bin/env python3
import binascii
import serial
import time
import sys

def send_sync(s):
	for tryiter in range(3):
		blob = b'\x55\x55\x55\x55'
		s.write(blob)
		rdbl = s.read(len(blob))
		if rdbl == blob:
			return True
		print('Echo failed!')
		print('      Sent:', binascii.hexlify(blob))
		print('  Received:', binascii.hexlify(rdbl))
		# flush
		s.read(1024)
	print('Fatal echo error!')
	return False


if __name__ == '__main__':
	if len(sys.argv) > 1:
		port = sys.argv[1]
	else:
		port = '/dev/ttyUSB0'

	s = serial.Serial(port, 9600)

	print('Calibrating...')

	# wait for hw to boot, and flush received garbage
	s.timeout = 0.2
	s.read(1024)

	cals = []

	for i in range(100):
		print('Iter #%d...' % i)
		if not send_sync(s):
			sys.exit(1)
			break
		resp = s.readline().decode(errors="ignore")
		if resp == '':
			print('No response. Device dead?')
			continue

		try:
			err, cal = [int(x, 16) for x in resp.split(' ')]
		except ValueError:
			print('Undecodeable data:', resp)
			continue
		print('Delta: %d, Cal: 0x%02X' % (err, cal))

		cals.append(cal)
		if len(cals) > 4:
			del cals[0]
		if (len(cals) == 4 and len(set(cals)) <= 2) or err == 0:
			print('Done!')
			break

		time.sleep(0.1)

	sys.exit(0)
