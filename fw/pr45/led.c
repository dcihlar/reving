#include <avr/io.h>

#include "led.h"


void led_init(void) {
	DDRA |= _BV(PA1);
	PORTA |= _BV(PA1);
}

void led_toggle(void) {
	PORTA ^= _BV(PA1);
}
