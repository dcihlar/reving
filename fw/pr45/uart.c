#include <stdbool.h>

#include <avr/io.h>
#include <util/delay.h>

#include <reving/uart.h>
#include "uart.h"
#include "led.h"


static bool was_reading = false;
static bool was_writing = false;


void uart_init(void) {
#define BAUD	BAUD_RATE
#include <util/setbaud.h>
	UBRRL = UBRRL_VALUE;
	UBRRH = UBRRH_VALUE;
#if USE_2X
	UCSRA |= _BV(U2X);
#else
	UCSRA &= ~_BV(U2X);
#endif
	UCSRB = 0x00;
	UCSRC = _BV(UCSZ1) | _BV(UCSZ0);

	DDRD &= ~(_BV(PD0) | _BV(PD1));
	PORTD |= _BV(PD0);

	was_reading = false;
	was_writing = false;
}

void uart_writeb(uint8_t c) {
	if (!was_writing) {
		_delay_ms(1);
		was_reading = false;
		was_writing = true;
		led_toggle();
		UCSRB = _BV(TXEN);
	}
	UDR = c;
	loop_until_bit_is_set(UCSRA, TXC);
	UCSRA |= _BV(TXC);
}

uint8_t uart_readb(void) {
	if (!was_reading) {
		was_reading = true;
		was_writing = false;
		led_toggle();
		UCSRB = _BV(RXEN);
	}
	loop_until_bit_is_set(UCSRA, RXC);
	return UDR;
}
