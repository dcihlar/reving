#include <stdint.h>
#include <stdbool.h>

#include <avr/interrupt.h>
#include <util/delay.h>

#include <reving/uart.h>
#include "osccal.h"
#include "uart.h"
#include "led.h"

#define UART_MASK		_BV(PD0)
#define TWO_BIT_CYCLES	((uint32_t)(2 * F_CPU + BAUD_RATE / 2) / BAUD_RATE)
#define AVG_COUNT		19

static volatile bool stopwatch_done, stopwatch_failed;
static uint8_t stopwatch_high;
static uint8_t bitcount;

static uint16_t abs(int16_t i) {
	if (i < 0) return -i;
	return i;
}

static void stopwatch_stop() {
	stopwatch_done = true;
	GIMSK &= ~_BV(PCIE2);
}

static void falling_edge_isr(void) {
	led_toggle();
	if (bitcount == 0) {
		TCCR0B |= _BV(CS00);
	} else if (bitcount == AVG_COUNT) {
		TCCR0B &= ~_BV(CS00);
		stopwatch_stop();
	}
	++bitcount;
}

ISR(TIMER0_OVF_vect) {
	++stopwatch_high;
	if (!stopwatch_high) {
		stopwatch_failed = true;
		stopwatch_stop();
	}
}

ISR(PCINT2_vect) {
	// detect falling edge on rxd
	if ((PIND & UART_MASK) != UART_MASK)
		falling_edge_isr();
}

static void stopwatch_prepare(void) {
	// init timer
	TCCR0A = 0x00;
	TCCR0B = 0x00;
	stopwatch_high = 0;
	TCNT0 = 0;
	TIMSK = _BV(TOIE0);
	
	// start stopwatch
	GIFR |= _BV(PCIF2);			// clear old flag
	GIMSK |= _BV(PCIE2);		// start PCINT
	stopwatch_done = false;		// not done
	stopwatch_failed = false;	// not failed
	bitcount = 0;				// start counting bits
	sei();						// enable interrupts
}

static uint16_t stopwatch_wait(void) {
	while (!stopwatch_done);
	cli();
	if (stopwatch_failed)
		return 0xFFFF;
	return ((uint16_t)stopwatch_high << 8) | TCNT0;
}

static void putnibble(uint8_t n) {
	uart_writeb(((n < 10) ? '0' : 'A'-10) + n);
}

static void puthex(uint8_t x) {
	putnibble((x >> 4) & 0x0F);
	putnibble((x >> 0) & 0x0F);
}

static void burst(void) {
	// generate F_OSC / 4 on LED pin
	PINA |= _BV(PA1);
	PINA |= _BV(PA1);
	PINA |= _BV(PA1);
	PINA |= _BV(PA1);
	PINA |= _BV(PA1);
	PINA |= _BV(PA1);
	PINA |= _BV(PA1);
	PINA |= _BV(PA1);
}

int main() {
	uart_init();
	led_init();
	led_toggle();

	PCMSK2 = UART_MASK;	// watch for change on UART rxd

	_delay_ms(100);

	int16_t best_error = 0x7FFF;
	int16_t this_error;

	for (;;) {
		uint16_t res;

		do {
			stopwatch_prepare();
			res = stopwatch_wait();
		} while (res >= 0x7FFF);

		burst();
		_delay_ms((10 * 1000 + BAUD_RATE / 2) / BAUD_RATE);

		this_error = (int16_t)res - (int16_t)(TWO_BIT_CYCLES * AVG_COUNT);
		uint16_t this_abs_error = abs(this_error);
		if (this_error < 0) uart_writeb('-');
		puthex(this_abs_error >> 8);
		puthex(this_abs_error & 0xFF);
		uart_writeb(' ');
		puthex(OSCCAL);
		uart_writeb('\r');
		uart_writeb('\n');
		uart_init();

		if (abs(best_error) > this_abs_error) {
			best_error = this_error;
			osc_calibration_save();
		}

		if (this_error < 0) {
			OSCCAL++;
		} else if (this_error > 0) {
			OSCCAL--;
		}
	}

	return 0;
}
