#include <avr/eeprom.h>
#include <avr/io.h>

#include <reving/addr.h>
#include "osccal.h"


static uint8_t EEMEM addr;
static uint8_t EEMEM osccal = 0xFF;

uint8_t reving_get_address(void) {
	return eeprom_read_byte(&addr);
}

void reving_set_address(uint8_t new_addr) {
	eeprom_update_byte(&addr, new_addr);
}

void osc_calibrate(void) {
	uint8_t ee_osccal = eeprom_read_byte(&osccal);
	if (ee_osccal != 0xFF)
		OSCCAL = ee_osccal;
}

void osc_calibration_save(void) {
	eeprom_update_byte(&osccal, OSCCAL);
}
