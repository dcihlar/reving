#include <stdbool.h>

#include <avr/io.h>
#include <util/delay.h>

#include <reving/uart.h>
#include "uart.h"
#include "led.h"


static bool was_reading = false;


void uart_init(void) {
#define BAUD	9600
#include <util/setbaud.h>
	UBRR0 = UBRR_VALUE;
#if USE_2X
	UCSR0A |= _BV(U2X0);
#else
	UCSR0A &= ~_BV(U2X0);
#endif
	UCSR0B = _BV(RXEN0);
	UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);

	DDRE &= ~(_BV(PE0) | _BV(PE1));
	PORTE |= _BV(PE0);
}

void uart_writeb(uint8_t c) {
	if (was_reading) {
		_delay_ms(1);
		was_reading = false;
		led_toggle();
		UCSR0B = _BV(TXEN0);
	}
	UDR0 = c;
	loop_until_bit_is_set(UCSR0A, TXC0);
	UCSR0A |= _BV(TXC0);
}

uint8_t uart_readb(void) {
	if (!was_reading) {
		was_reading = true;
		led_toggle();
		UCSR0B = _BV(RXEN0);
	}
	loop_until_bit_is_set(UCSR0A, RXC0);
	return UDR0;
}
