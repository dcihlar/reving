#ifndef _LED_TOGGLE_H_
#define _LED_TOGGLE_H_

void led_init(void);
void led_toggle(void);

#endif
