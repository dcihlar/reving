#include <avr/io.h>

#include <reving/reving.h>
#include "pins.h"


void pins_init(void) {
	// p1..p8 (0..7)
	DDRD = 0x00;
	PORTD = 0xFF;
	// p9..p16 (8..15)
	DDRC = 0x00;
	PORTC = 0xFF;
	// p17..p24 (16..23)
	DDRA = 0x00;
	PORTA = 0xFF;
	// p25..p32 (24..31)
	DDRF = 0x00;
	PORTF = 0xFF;
	// p33 (32)
	DDRE &= ~_BV(PE2);
	PORTE |= _BV(PE2);
}

uint8_t reving_get_num_pins(void) {
	return 33;
}

#define OUT(ddr, port, pmask, mode) \
	switch (mode) { \
		case REVING_PIN_0: \
			port &= ~pmask; \
		case REVING_PIN_1: \
			ddr |= pmask; \
			break; \
		case REVING_PIN_IN: \
			ddr &= ~pmask; \
			port |= pmask; \
			break; \
	}

void reving_cmd_output(uint8_t pin, reving_pin_mode_t mode) {
	uint8_t pmask;
	if (pin < 8) {
		pmask = _BV(pin);
		OUT(DDRD, PORTD, pmask, mode);
	} else if (pin < 16) {
		pmask = _BV(pin - 8);
		OUT(DDRC, PORTC, pmask, mode);
	} else if (pin < 24) {
		pmask = _BV(pin - 16);
		OUT(DDRA, PORTA, pmask, mode);
	} else if (pin < 32) {
		pmask = _BV(pin - 24);
		OUT(DDRF, PORTF, pmask, mode);
	} else {
		pmask = _BV(PE2);
		OUT(DDRE, PORTE, pmask, mode);
	}
}

void reving_cmd_read(uint8_t *pin_data) {
	pin_data[0] =               ((PIND << 0) & 0x7F);
	pin_data[1] = (PIND >> 7) | ((PINC << 1) & 0x7E);
	pin_data[2] = (PINC >> 6) | ((PINA << 2) & 0x7C);
	pin_data[3] = (PINA >> 5) | ((PINF << 3) & 0x78);
	pin_data[4] = (PINF >> 4) | ((PINE << 2) & 0x10);
}
