#include <stdbool.h>

#include <reving/addr.h>
#include <reving/uart.h>
#include "proto.h"


#define IS_HDR(x)	((x) & 0x80)
#define HDR_ADDR(x)	((x) & 0x7F)


struct _reving_msg_t reving_msg;


static bool reving_try_read_data(uint8_t *data) {
	*data = uart_readb();
	return !IS_HDR(*data);
}

static bool reving_try_read_header(uint8_t *hdr) {
	*hdr = uart_readb();
	return IS_HDR(*hdr);
}

reving_msg_stat_t reving_try_read_msg(void) {
	uint8_t b;

	if (!reving_try_read_header(&b))
		return REVING_MSG_STAT_NOT_HEADER;

	if ((HDR_ADDR(b) != 0) &&
	    (HDR_ADDR(b) != reving_get_address()))
		return REVING_MSG_STAT_NOT_FOR_ME;

	if (!reving_try_read_data(&b))
		return REVING_MSG_STAT_TRUNCATED;

	reving_msg.cmd = (b >> 3) & 0xF;
	reving_msg.nargs = b & 7;

	for (uint8_t i = 0 ; i < reving_msg.nargs ; ++i) {
		if (!reving_try_read_data(&reving_msg.args[i]))
			return REVING_MSG_STAT_TRUNCATED;
	}

	return REVING_MSG_STAT_OK;
}

void reving_read_msg(void) {
	while (reving_try_read_msg() != REVING_MSG_STAT_OK);
}

void reving_send_msg(void) {
	reving_msg.nargs &= 7;
	uart_writeb(((reving_msg.cmd & 0xF) << 3) | reving_msg.nargs);
	for (uint8_t i = 0 ; i < reving_msg.nargs ; ++i) {
		uart_writeb(reving_msg.args[i] & 0x7F);
	}
}
