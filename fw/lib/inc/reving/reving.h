#ifndef _REVING_REVING_H_
#define _REVING_REVING_H_

#include <stdint.h>

typedef enum {
	REVING_PIN_0 = 0,
	REVING_PIN_1,
	REVING_PIN_IN,
} reving_pin_mode_t;

void reving_task(void);
void reving_cmd_read(uint8_t *pin_data);
void reving_cmd_output(uint8_t pin, reving_pin_mode_t mode);
uint8_t reving_get_num_pins(void);

#endif
