#ifndef _REVING_ADDR_H_
#define _REVING_ADDR_H_

#include <stdint.h>

uint8_t reving_get_address(void);
void reving_set_address(uint8_t addr);

#endif
