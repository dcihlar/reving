#ifndef _REVING_UART_H_
#define _REVING_UART_H_

#include <stdint.h>

uint8_t uart_readb(void);
void uart_writeb(uint8_t b);

#endif
