#ifndef _REVING_PROTO_H_
#define _REVING_PROTO_H_

#include <stdint.h>

extern struct _reving_msg_t {
	uint8_t cmd;
	uint8_t nargs;
	uint8_t args[7];
} reving_msg;

typedef enum {
	REVING_MSG_STAT_OK = 0,
	REVING_MSG_STAT_NOT_FOR_ME,
	REVING_MSG_STAT_NOT_HEADER,
	REVING_MSG_STAT_TRUNCATED,
} reving_msg_stat_t;

reving_msg_stat_t reving_try_read_msg(void);
void reving_read_msg(void);
void reving_send_msg(void);

#endif
