#include <reving/reving.h>
#include <reving/addr.h>

#include "proto.h"


#define CMD_RESET	0x00
#define CMD_PINCOUNT	0x01
#define CMD_READ	0x02
#define CMD_OUTPUT	0x03
#define CMD_NEXT_OUT	0x04
#define CMD_SET_ADDR	0x05


static void reving_cmd_reset(void) {
	for (int i = 0 ; i < reving_get_num_pins() ; ++i)
		reving_cmd_output(i, REVING_PIN_IN);
}

static void _reving_cmd_output(uint8_t pin, uint8_t mode) {
	if ((pin < reving_get_num_pins()) &&
	    (mode <= 2)) {
		reving_cmd_output(pin, mode);
	}
}

void reving_task() {
	reving_read_msg();

	switch (reving_msg.cmd) {
		case CMD_RESET:
			reving_cmd_reset();
			break;
		case CMD_PINCOUNT:
			reving_msg.nargs = 1;
			reving_msg.args[0] = reving_get_num_pins();
			reving_send_msg();
			break;
		case CMD_READ:
			reving_msg.nargs = (reving_get_num_pins() + 6) / 7;
			reving_cmd_read(reving_msg.args);
			reving_send_msg();
			break;
		case CMD_NEXT_OUT:
			reving_cmd_reset();
			// fall through
		case CMD_OUTPUT:
			if (reving_msg.nargs == 1)
				reving_msg.args[1] = REVING_PIN_0;
			_reving_cmd_output(reving_msg.args[0], reving_msg.args[1]);
			break;
		case CMD_SET_ADDR:
			reving_set_address(reving_msg.args[0]);
			break;
	}
}
