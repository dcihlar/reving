#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <reving/addr.h>
#include <reving/uart.h>
#include <reving/reving.h>

#include "util.h"


static const uint8_t *uart_fake_tx_data;
static uint8_t uart_rx_data[16];
static int uart_rx_data_ofs;
static bool was_rx_cmp = false;
static uint8_t addr = 4;


uint8_t uart_readb(void) {
	if (!uart_fake_tx_data) {
		fprintf(stderr, "Internal error: missing tx data!\n");
		exit(1);
	}
	return *uart_fake_tx_data++;
}

void uart_fake_tx(const uint8_t *ftx) {
	uart_fake_tx_data = ftx;
}

void uart_writeb(uint8_t b) {
	if (was_rx_cmp) {
		uart_rx_data_ofs = 0;
		was_rx_cmp = false;
	}
	if (uart_rx_data_ofs >= sizeof(uart_rx_data)) {
		fprintf(stderr, "Fatal error: Sending too large messages!\n");
		exit(1);
	}
	uart_rx_data[uart_rx_data_ofs++] = b;
}

bool uart_cmp_rx(const uint8_t *data, int size) {
	was_rx_cmp = true;
	return size == uart_rx_data_ofs && !memcmp(data, uart_rx_data, size);
}

const char * uart_rx_to_str(void) {
	return blob_to_str(uart_rx_data, uart_rx_data_ofs);
}

uint8_t reving_get_address(void) {
	return addr;
}

void reving_set_address(uint8_t new_addr) {
	addr = new_addr;
}
