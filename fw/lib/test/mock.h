#ifndef _MOCK_H_
#define _MOCK_H_

#include <stdint.h>
#include <stdbool.h>

#define UART_TX(...)	uart_fake_tx((uint8_t[]){__VA_ARGS__})
#define UART_RX(...)	uart_cmp_rx((uint8_t[]){__VA_ARGS__}, sizeof((uint8_t[]){__VA_ARGS__}))

void uart_fake_tx(const uint8_t *ftx);
bool uart_cmp_rx(const uint8_t *data, int size);
const char * uart_rx_to_str(void);

#endif
