#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdint.h>


#define BLOB_TO_STR(...)	blob_to_str((uint8_t[]){__VA_ARGS__}, sizeof((uint8_t[]){__VA_ARGS__}))


const char * blob_to_str(const uint8_t *blob, int blob_len);

#endif
