#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mock.h"
#include "util.h"
#include "pins.h"
#include "../proto.h"
#include <reving/reving.h>


static int testcnt = 0;
#define CHECK_EQ(a, b) \
	++testcnt; \
	if ((a) != (b)) { \
		fprintf(stderr, "FAILED test %d in line %d\n", testcnt, __LINE__); \
		exit(1); \
	} else { \
		fprintf(stderr, "OK test %d\n", testcnt); \
	}
#define CHECK_RX(...) \
	++testcnt; \
	if (!UART_RX(__VA_ARGS__)) { \
		fprintf(stderr, "FAILED rx %d in line %d\n", testcnt, __LINE__); \
		fprintf(stderr, "  %s\n    !=\n", BLOB_TO_STR(__VA_ARGS__)); \
		fprintf(stderr, "  %s\n", uart_rx_to_str()); \
		exit(1); \
	} else { \
		fprintf(stderr, "OK rx %d\n", testcnt); \
	}
#define CHECK_PINS(...) \
	UART_TX(0x84, 0x10); \
	reving_task(); \
	CHECK_RX(0x12, __VA_ARGS__);


void test_proto(void) {
	// in the middle of communication
	UART_TX(0x00);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_NOT_HEADER);
	// wrong address
	UART_TX(0x81);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_NOT_FOR_ME);
	// no cmd
	UART_TX(0x84, 0x84);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_TRUNCATED);
	// no first arg
	UART_TX(0x84, 0x02, 0x84);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_TRUNCATED);
	// no last arg
	UART_TX(0x84, 0x02, 0x00, 0x84);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_TRUNCATED);
	// complete msg no args
	UART_TX(0x84, 0x00);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_OK);
	// complete msg with args
	UART_TX(0x84, 0x02, 0x00, 0x00);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_OK);
	// complete 2 msgs
	UART_TX(0x84, 0x00, 0x84, 0x00);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_OK);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_OK);
	// complete msg + fail + complete msg
	UART_TX(0x84, 0x00, 0x81, 0x00, 0x84, 0x00);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_OK);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_NOT_FOR_ME);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_NOT_HEADER);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_OK);
	// check parsing
	UART_TX(0x84, 0x27, 1, 2, 3, 4, 5, 6, 7);
	reving_read_msg();
	CHECK_EQ(reving_msg.cmd, 4);
	CHECK_EQ(reving_msg.nargs, 7);
	for (int i = 0 ; i < reving_msg.nargs ; ++i) {
		CHECK_EQ(reving_msg.args[i], i + 1);
	}
	// check read with garbage
	UART_TX(0x00, 0x81, 0x84, 0x20, 0x84, 0x30, 0x00, 0x84, 0x40);
	reving_read_msg();
	CHECK_EQ(reving_msg.cmd, 4);
	reving_read_msg();
	CHECK_EQ(reving_msg.cmd, 6);
	reving_read_msg();
	CHECK_EQ(reving_msg.cmd, 8);
}

void test_cmds(void) {
	// get pin count
	UART_TX(0x84, 0x08);
	reving_task();
	CHECK_RX(0x09, 8);

	// get pins states
	CHECK_PINS(0x7F, 0x01);
	set_conn(0, '0');
	CHECK_PINS(0x7E, 0x01);
	set_conn(7, '0');
	CHECK_PINS(0x7E, 0x00);

	// output 0 on a pin
	UART_TX(0x84, 0x1A, 1, 0);
	reving_task();
	CHECK_PINS(0x7C, 0x00);
	// revert it back to input
	UART_TX(0x84, 0x1A, 1, 2);
	reving_task();
	CHECK_PINS(0x7E, 0x00);

	// reset (make some pins output and then reset them back)
	UART_TX(0x84, 0x1A, 1, 0,
	        0x84, 0x1A, 2, 0);
	reving_task();
	reving_task();
	CHECK_PINS(0x78, 0x00);
	UART_TX(0x84, 0x00);
	reving_task();
	CHECK_PINS(0x7E, 0x00);

	// out next
	UART_TX(0x84, 0x22, 1, 0);
	reving_task();
	CHECK_PINS(0x7C, 0x00);
	UART_TX(0x84, 0x21, 2);
	reving_task();
	CHECK_PINS(0x7A, 0x00);
	UART_TX(0x84, 0x21, 3);
	reving_task();
	CHECK_PINS(0x76, 0x00);
	UART_TX(0x84, 0x00);	// reset
	reving_task();
	CHECK_PINS(0x7E, 0x00);

	// access pin out of bounds
	UART_TX(0x84, 0x1A, 44, 0);
	reving_task();
	CHECK_PINS(0x7E, 0x00);

	// invalid mode
	UART_TX(0x84, 0x1A, 0, 3);
	reving_task();
	CHECK_PINS(0x7E, 0x00);
}

void test_addressing(void) {
	// broadcast
	UART_TX(0x80, 0x00);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_OK);

	// change to 8
	UART_TX(0x80, 0x29, 8);
	reving_task();
	UART_TX(0x88, 0x00);
	CHECK_EQ(reving_try_read_msg(), REVING_MSG_STAT_OK);
	// revert it back to 4
	UART_TX(0x80, 0x29, 4);
	reving_task();
}

int main() {
	test_proto();
	test_cmds();
	test_addressing();

	fprintf(stderr, "All OK!\n");
	return 0;
}
