#include <stdio.h>
#include <stdint.h>

#include "util.h"

static char buf[256];


const char * blob_to_str(const uint8_t *blob, int blob_len) {
	for (int i = 0, ofs=0 ; i < blob_len ; ++i, ofs+=3) {
		snprintf(buf+ofs, sizeof(buf)-ofs, "%02X ", *blob++);
	}
	return buf;
}
