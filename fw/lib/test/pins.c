#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <reving/reving.h>
#include "pins.h"


static char pins_state[] = "rrrrrrrr";
static char conn_state[] = "zzzzzzzz";


uint8_t reving_get_num_pins(void) {
	return strlen(pins_state);
}

static bool merge_state(char pin, char conn) {
	if (pin == 'r') {
		return (conn == '1') || (conn == 'z');
	} else if (conn == 'z') {
		return pin == '1';
	} else {
		fprintf(stderr, "Fatal error: connected pin=%c with conn=%c!\n", pin, conn);
		exit(1);
	}
}

void set_conn(int pin, char state) {
	if (pin < 0 || pin >= strlen(pins_state)) {
		fprintf(stderr, "Internal error: invalid conn pin number!\n");
		exit(1);
	}
	if (state != 'z' && state != '1' && state != '0') {
		fprintf(stderr, "Internal error: invalid conn pin state!\n");
		exit(1);
	}
	conn_state[pin] = state;
}

void reving_cmd_read(uint8_t *pin_data) {
	memset(pin_data, 0, 7);
	for (int i = 0 ; i < strlen(pins_state) ; ++i) {
		pin_data[i / 7] |= merge_state(pins_state[i], conn_state[i]) << (i % 7);
	}
}

void reving_cmd_output(uint8_t pin, reving_pin_mode_t mode) {
	if (pin > strlen(pins_state)) {
		fprintf(stderr, "Internal error: invalid pin!\n");
		exit(1);
	}
	switch (mode) {
		case REVING_PIN_0:
			pins_state[pin] = '0';
			break;
		case REVING_PIN_1:
			pins_state[pin] = '1';
			break;
		case REVING_PIN_IN:
			pins_state[pin] = 'r';
			break;
		default:
			fprintf(stderr, "Internal error: invalid pin mode!\n");
			break;
	}
}
