from unittest.mock import Mock, call
import unittest

from reving.tests.busmock import BusMock
from reving.reving import *
from reving.proto import *


class TestPort(unittest.TestCase):
    def setUp(self):
        self.bus = BusMock()
        self.bus.add_fake_response(None, CmdId.PINCOUNT, Response(0, 10))
        self.bus.add_fake_response(None, CmdId.READ, Response(0, 0x5A, 0x01))
        self.port = Port(self.bus, 1)

    def test_reset(self):
        self.port.reset()
        self.bus.send.assert_called_once_with(1, Command(CmdId.RESET))

    def test_get_pin_no(self):
        self.assertEqual(self.port.num_pins(), 10)
        self.assertEqual(self.port.num_pins(), 10)
        self.bus.send.assert_called_once_with(1, Command(CmdId.PINCOUNT))
        self.bus.recv.assert_called_once()

    def test_read(self):
        self.assertEqual(self.port.read(), [False, True, False, True, True, False, True, True, False, False])

    def test_output_high(self):
        self.port.output(2, True)
        self.bus.send.assert_any_call(1, Command(CmdId.OUTPUT, 2, PinMode.HIGH))

    def test_output_low(self):
        self.port.output(4, False)
        self.bus.send.assert_any_call(1, Command(CmdId.OUTPUT, 4))
        self.bus.send.reset_mock()
        self.port.output(4)
        self.bus.send.assert_any_call(1, Command(CmdId.OUTPUT, 4))

    def test_output_in(self):
        self.port.output(8, None)
        self.bus.send.assert_any_call(1, Command(CmdId.OUTPUT, 8, PinMode.INPUT))

    def test_output_bad_pin(self):
        with self.assertRaises(ValueError):
            self.port.output(11, True)

    def test_try_next(self):
        self.port.try_next()
        self.port.try_next()
        self.bus.send.assert_has_calls([
            call(1, Command(CmdId.NEXT_OUT, 0)),
            call(1, Command(CmdId.NEXT_OUT, 1)),
        ])

    def test_try_next_direct(self):
        self.port.try_next(2)
        self.bus.send.assert_any_call(1, Command(CmdId.NEXT_OUT, 2))

    def test_rewind(self):
        self.port.try_next()
        self.port.rewind()
        self.port.try_next()
        self.bus.send.assert_has_calls([
            call(1, Command(CmdId.NEXT_OUT, 0)),
            call(1, Command(CmdId.NEXT_OUT, 0)),
        ])

    def test_try_next_overflow(self):
        for i in range(10):
            self.port.try_next()
        with self.assertRaises(RuntimeError):
            self.port.try_next()

    def test_try_next_direct_overflow(self):
        with self.assertRaises(ValueError):
            self.port.try_next(100)

    def test_set_addr(self):
        self.port.set_addr(8)
        self.bus.send.assert_any_call(1, Command(CmdId.SET_ADDR, 8))
        self.assertEqual(self.port.addr, 8)

    def test_set_bad_addr(self):
        with self.assertRaises(ValueError):
            self.port.set_addr(0)   # broadcast
        with self.assertRaises(ValueError):
            self.port.set_addr(256) # out of range
        with self.assertRaises(ValueError):
            self.port.set_addr(-1)  # out of range
        with self.assertRaises(TypeError):
            self.port.set_addr('s') # invalid type


class TestBusConnection(unittest.TestCase):
    def setUp(self):
        self.bus = Bus()

    def simulate_connect(self, read_data=None):
        def fake_echo(data):
            self.bus.s.read.return_value = data
        self.bus.s = Mock()
        if read_data is None:
            self.bus.s.write.side_effect = fake_echo
        else:
            self.bus.s.read.return_value = bytes.fromhex(read_data)

    def test_not_connected(self):
        with self.assertRaises(BusError):
            self.bus.send(1, Command())

    def test_connected(self):
        self.simulate_connect()
        self.bus.send(1, Command())
        self.bus.s.write.assert_called_once()

    def test_send(self):
        self.simulate_connect()
        cmd = Command(4, 8)
        self.bus.send(1, cmd)
        self.bus.s.write.assert_called_once_with(cmd.compile(1))

    def test_recv(self):
        self.simulate_connect('1222')
        self.assertEqual(self.bus.recv(), Response(2, 0x12, 0x22))

    def test_conflict(self):
        self.simulate_connect('deadbeef')
        with self.assertRaises(BusError):
            self.bus.send(1, Command())

    def test_no_device(self):
        self.simulate_connect('')
        with self.assertRaises(NoDevice):
            self.bus.recv()

    def test_bad_data(self):
        self.simulate_connect('8401')
        with self.assertRaises(BusError):
            self.bus.recv()


class TestBusPorts(unittest.TestCase):
    def setUp(self):
        self.bus = BusMock()
        for addr in [0, 1, 2, 4]:
            self.bus.add_fake_response(addr, CmdId.PINCOUNT, Response(0, 10))

    def test_same_port(self):
        p1a = self.bus.get_port(1)
        p1b = self.bus.get_port(1)
        self.assertIs(p1a, p1b)

    def test_diff_ports(self):
        p1 = self.bus.get_port(1)
        p2 = self.bus.get_port(2)
        self.assertIsNot(p1, p2)

    def test_missing_port(self):
        with self.assertRaises(NoDevice):
            self.bus.get_port(3)
        self.bus.get_port(4)

    def test_invalid_port(self):
        with self.assertRaises(ValueError):
            self.bus.get_port(0)
        with self.assertRaises(ValueError):
            self.bus.get_port(256)
        with self.assertRaises(TypeError):
            self.bus.get_port('s')

    def test_rename(self):
        p1 = self.bus.get_port(1)
        p1.set_addr(2)
        p2 = self.bus.get_port(2)
        self.assertIs(p1, p2)

    def test_rename_broadcast(self):
        pb = Port(self.bus, 0)
        pb.set_addr(4)
        p4 = self.bus.get_port(4)
        self.assertIs(pb, p4)

    def test_rename_duplicate(self):
        self.bus.get_port(2)
        p4 = self.bus.get_port(4)
        with self.assertRaises(BusError):
            p4.set_addr(2)

    def test_reset(self):
        self.bus.reset()
        self.bus.send.assert_called_once_with(0, Command(CmdId.RESET))
