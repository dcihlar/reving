from unittest.mock import Mock

from reving.reving import BusBase, NoDevice


class BusMock(BusBase):
    send = None
    recv = None

    def __init__(self):
        super().__init__()
        self.s = True
        self.send = Mock()
        self.recv = Mock()
        self.send.side_effect = self._fake_send
        self.fake_resps = {}
        self.fake_addrs = set()

    def _fake_send(self, addr, cmd):
        cmd_id = cmd.cmd
        resp = self.fake_resps.get((addr, cmd_id), None)
        if resp is None:
            resp = self.fake_resps.get((None, cmd_id), None)
        if resp is None and None not in self.fake_addrs and addr not in self.fake_addrs:
            raise NoDevice()
        self.recv.return_value = resp

    def add_fake_response(self, addr, cmd_id, resp):
        self.fake_resps[(addr, cmd_id)] = resp
        self.fake_addrs.add(addr)
