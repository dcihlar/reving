from io import StringIO
from unittest.mock import Mock
import unittest

from reving.tests.busmock import BusMock
from reving.reving import CmdId, Response
from reving.simbus import SimBus
from reving.ubernetlist import *


class TestInit(unittest.TestCase):
    def setUp(self):
        self.bus = Mock()
        self.unl = UberNetList(self.bus)

    def test_reset(self):
        self.bus.reset.assert_called_once()


class TestLoad(unittest.TestCase):
    def setUp(self):
        self.bus = BusMock()
        self.bus.add_fake_response(0, CmdId.RESET, Response())
        self.bus.add_fake_response(1, CmdId.PINCOUNT, Response(0, 4))
        self.bus.add_fake_response(2, CmdId.PINCOUNT, Response(0, 8))
        self.unl = UberNetList(self.bus)

    def test_load_ok(self):
        jss = StringIO('''
        {
            "X0": {
                "config": {
                    "addr": 1
                },
                "pins": { "1": "N0", "2": null, "3": null, "4": null }
            },
            "X1": {
                "config": {
                    "addr": 2
                },
                "pins": { "1": "N0", "2": null, "3": null, "4": null, "5": null, "6": null, "7": null, "8": null }
            }
        }
        ''')
        self.unl.load(jss)

    def test_load_duplicate_addrs(self):
        jss = StringIO('''
        {
            "X0": {
                "config": {
                    "addr": 1
                },
                "pins": { "1": "N0", "2": null, "3": null, "4": null }
            },
            "X1": {
                "config": {
                    "addr": 1
                },
                "pins": { "1": "N0", "2": null, "3": null, "4": null }
            }
        }
        ''')
        with self.assertRaises(UberNetListError):
            self.unl.load(jss)

    def test_load_missing_hw(self):
        jss = StringIO('''
        {
            "X0": {
                "config": {
                    "addr": 3
                },
                "pins": { "1": "N0", "2": null, "3": null, "4": null }
            }
        }
        ''')
        with self.assertRaises(UberNetListError):
            self.unl.load(jss)

    def test_load_inval_pins(self):
        jss = StringIO('''
        {
            "X0": {
                "config": {
                    "addr": 1
                },
                "pins": { "1": "N0", "2": null, "3": null }
            }
        }
        ''')
        with self.assertRaises(UberNetListError):
            self.unl.load(jss)

    def test_auto_init(self):
        self.unl.auto_init()
        x1 = self.unl._ref_to_port('X1')
        self.assertEqual(x1.config['addr'], 1)
        self.assertEqual(x1.num_pins(), 4)
        x2 = self.unl._ref_to_port('X2')
        self.assertEqual(x2.config['addr'], 2)
        self.assertEqual(x2.num_pins(), 8)


class TestScan(unittest.TestCase):
    def setUp(self):
        nl = NetList()
        ports = list(nl.add_port(4) for i in range(3))
        for port_addr, port in enumerate(ports, 1):
            port.config['addr'] = port_addr
        ports[0][0].connect(ports[1][0])
        ports[1][1].connect(ports[2][1])
        ports[0][2].connect(ports[2][2])
        ports[0][3].connect(ports[0][0])
        self.sb = SimBus(nl)
        self.unl = UberNetList(self.sb)
        self.unl.auto_init()
        self.unl.scan()

    def test_ports(self):
        self.assertEqual(len(self.unl.ports), 3)

    def test_nets(self):
        self.assertEqual(len(self.unl.nets), 3)

    def test_same_port_connection(self):
        x1 = self.unl.ports['X1']
        self.assertTrue(x1[3].is_connected_with(x1[0]))

    def test_cross_connections(self):
        x1 = self.unl.ports['X1']
        x2 = self.unl.ports['X2']
        x3 = self.unl.ports['X3']
        self.assertTrue(x1[0].is_connected_with(x2[0]))
        self.assertTrue(x2[1].is_connected_with(x3[1]))
        self.assertTrue(x1[2].is_connected_with(x3[2]))
        self.assertFalse(x1[0].is_connected_with(x2[1]))
        self.assertFalse(x2[1].is_connected_with(x1[2]))
        self.assertFalse(x1[0].is_connected_with(x1[2]))


class TestActivePins(unittest.TestCase):
    def setUp(self):
        self.nl = NetList()
        ports = list(self.nl.add_port(4) for i in range(3))
        for port_addr, port in enumerate(ports, 1):
            port.config['addr'] = port_addr
        ports[0][0].connect(ports[1][0])
        ports[1][1].connect(ports[2][1])
        ports[0][2].connect(ports[2][2])
        self.ports = ports
        self.sb = SimBus(self.nl)
        self.unl = UberNetList(self.sb)
        self.unl.auto_init()

    def test_active_pins(self):
        self.assertEqual(self.unl.get_active_pins(), [])
        self.sb.get_port(1).try_next()
        act_pins = set(self.unl.get_active_pins())
        exp_pins = {self.unl.ports['X1'][0], self.unl.ports['X2'][0]}
        self.assertEqual(act_pins, exp_pins)
