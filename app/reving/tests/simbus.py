import unittest

from reving.netlist import NetList
from reving.simbus import *


class TestPinArray(unittest.TestCase):
    def setUp(self):
        self.pa = PinArray(4)

    def test_size(self):
        self.assertEqual(len(self.pa), 4)
        self.assertEqual(len(list(self.pa)), 4)
        cnt = 0
        for pv in self.pa:
            cnt += 1
        self.assertEqual(cnt, 4)

    def test_init(self):
        self.assertTrue(all(self.pa))

    def test_access(self):
        self.pa[0] = False
        self.assertFalse(self.pa[0])
        self.pa[0] = True
        self.assertTrue(all(self.pa))
        self.pa[1] &= False
        self.assertFalse(self.pa[1])

    def test_byte(self):
        self.pa[0] = False
        self.pa[2] = False
        self.assertEqual(self.pa.to_bytes(), bytearray([0x0A]))

    def test_bytes(self):
        pa = PinArray(9)
        self.assertEqual(pa.to_bytes(), bytearray([0x7F, 0x03]))
        pa[0] = False
        pa[8] = False
        self.assertEqual(pa.to_bytes(), bytearray([0x7E, 0x01]))


class TestSimBus(unittest.TestCase):
    def setUp(self):
        nl = NetList()

        p1 = nl.add_port(4)
        p1.config['addr'] = 1
        p2 = nl.add_port(8)
        p2.config['addr'] = 2

        p1[0].connect(p2[0])
        p1[3].connect(p2[7])

        self.sb = SimBus(nl)
        self.p1 = self.sb.get_port(1)
        self.p2 = self.sb.get_port(2)

    def test_ports(self):
        self.assertEqual(self.p1.num_pins(), 4)
        self.assertEqual(self.p2.num_pins(), 8)

    def test_idle_pins(self):
        self.assertTrue(all(self.p1.read()))
        self.assertTrue(all(self.p2.read()))

    def test_set_on_net(self):
        self.p1.output(0, False)
        self.assertEqual(self.p1.read(), [False] + [True]*3)
        self.assertEqual(self.p2.read(), [False] + [True]*7)
        self.p1.output(0, None)
        self.assertTrue(all(self.p1.read()))
        self.assertTrue(all(self.p2.read()))
        self.p2.output(7, False)
        self.assertEqual(self.p1.read(), [True]*3 + [False])
        self.assertEqual(self.p2.read(), [True]*7 + [False])

    def test_set_no_net(self):
        self.p1.output(1, False)
        self.assertEqual(self.p1.read(), [True, False] + [True]*2)
        self.assertTrue(all(self.p2.read()))
        self.p1.output(1, None)
        self.assertTrue(all(self.p1.read()))
        self.assertTrue(all(self.p2.read()))

    def test_reset_on_net(self):
        self.p1.output(0, False)
        self.p1.reset()
        self.assertTrue(all(self.p1.read()))
        self.assertTrue(all(self.p2.read()))

    def test_reset_no_net(self):
        self.p1.output(1, False)
        self.p1.reset()
        self.assertTrue(all(self.p1.read()))
        self.assertTrue(all(self.p2.read()))

    def test_next(self):
        self.p1.try_next()
        self.assertEqual(self.p1.read(), [False] + [True]*3)
        self.assertEqual(self.p2.read(), [False] + [True]*7)
        self.p1.try_next()
        self.assertEqual(self.p1.read(), [True, False, True, True])
        self.assertTrue(all(self.p2.read()))
        self.p1.try_next()
        self.assertEqual(self.p1.read(), [True, True, False, True])
        self.assertTrue(all(self.p2.read()))
        self.p1.try_next()
        self.assertEqual(self.p1.read(), [True]*3 + [False])
        self.assertEqual(self.p2.read(), [True]*7 + [False])
