import unittest

from reving.proto import *


class TestPacket(unittest.TestCase):
    def test_simple(self):
        pkt = Packet(4)
        self.assertEqual(pkt.compile(), bytes([0x20]))

    def test_args(self):
        pkt = Packet(4, 42)
        self.assertEqual(pkt.compile(), bytes([0x21, 42]))

    def test_invalid_command(self):
        with self.assertRaises(ValueError):
            Packet(16)
        with self.assertRaises(ValueError):
            Packet(-1)
        pkt = Packet()
        pkt.cmd = 16
        with self.assertRaises(ValueError):
            pkt.compile()

    def test_invalid_argument(self):
        with self.assertRaises(ValueError):
            Packet(4, -1)
        with self.assertRaises(ValueError):
            Packet(4, 128)
        pkt = Packet()
        pkt.args = [-1]
        with self.assertRaises(ValueError):
            pkt.compile()

    def test_too_many_arguments(self):
        with self.assertRaises(ValueError):
            Packet(0, range(9))
        pkt = Packet()
        pkt.args = list(range(9))
        with self.assertRaises(ValueError):
            pkt.compile()


class TestCommand(unittest.TestCase):
    def test_address(self):
        cmd = Command(4)
        self.assertEqual(cmd.compile(12), bytes([0x8C, 0x20]))

    def test_invalid_address(self):
        cmd = Command(4)
        with self.assertRaises(ValueError):
            cmd.compile(-1)
        with self.assertRaises(ValueError):
            cmd.compile(128)
