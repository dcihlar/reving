from io import StringIO
import unittest

from reving.netlist import *


class TestPinNames(unittest.TestCase):
    def setUp(self):
        self.nl = NetList()
        self.port = self.nl.add_port(4)

    def test_initial_name(self):
        for i in range(4):
            self.assertEqual(self.port[i].name, str(i+1))

    def test_rename_pin(self):
        self.port[0].name = 'test'
        self.assertEqual(self.port[0].name, 'test')

    def test_invalid_pin_name(self):
        with self.assertRaises(ValueError):
            self.port[0].name = 'te st'


class TestDeRef(unittest.TestCase):
    def setUp(self):
        self.nl = NetList()
        self.nl2 = NetList()
        self.ports = list(self.nl.add_port(4) for i in range(2))
        self.nets = list(self.nl.add_net() for i in range(2))
        self.ports[0][0].name = 'p0'
        self.nl.connect(self.nets[0], self.ports[0][0])
        self.ports2 = list(self.nl2.add_port(4) for i in range(2))
        self.nets2 = list(self.nl2.add_net() for i in range(2))
        self.nl2.connect(self.nets2[0], self.ports2[0][0])

    def test_pin_by_name(self):
        self.assertEqual(self.ports[0]['p0'], self.ports[0][0])

    def test_pin_by_num_name(self):
        self.assertEqual(self.ports[0]['1'], self.ports[0][0])

    def test_pin_by_inval_name(self):
        with self.assertRaises(ValueError):
            self.ports[0]['100']
        with self.assertRaises(ValueError):
            self.ports[0]['abc']

    def test_net_by_name(self):
        self.assertIs(self.nl._ref_to_net('N0'), self.nets[0])
        self.assertIsNot(self.nl._ref_to_net('N1'), self.nets[0])

    def test_net_by_inval_name(self):
        with self.assertRaises(ValueError):
            self.nl._ref_to_net('N10')

    def test_net_by_pin_num_name(self):
        self.assertIs(self.nl._ref_to_net('X0.1'), self.nets[0])

    def test_net_by_pin_name(self):
        self.assertIs(self.nl._ref_to_net('X0.p0'), self.nets[0])

    def test_net_by_inval_pin_name(self):
        with self.assertRaises(ValueError):
            self.nl._ref_to_net('X10.1')
        with self.assertRaises(ValueError):
            self.nl._ref_to_net('X0.10')
        with self.assertRaises(ValueError):
            self.nl._ref_to_net('X0.p1')
        self.assertIsNone(self.nl._ref_to_net('X0.2'))

    def test_net_by_pin(self):
        self.assertIs(self.nl._ref_to_net(self.ports[0][0]), self.nets[0])

    def test_net_by_inval_pin(self):
        self.assertIsNone(self.nl._ref_to_net(self.ports[0][1]))
        with self.assertRaises(ValueError):
            self.nl._ref_to_net(self.ports2[0][0])

    def test_net_by_net(self):
        self.assertIs(self.nl._ref_to_net(self.nets[0]), self.nets[0])

    def test_net_by_inval_net(self):
        with self.assertRaises(ValueError):
            self.nl._ref_to_net(self.nets2[0])

    def test_net_by_inval_ref(self):
        with self.assertRaises(TypeError):
            self.nl._ref_to_net(None)

    def test_port_by_name(self):
        self.assertIs(self.nl._ref_to_port('X0'), self.ports[0])
        self.assertIsNot(self.nl._ref_to_port('X1'), self.ports[0])

    def test_port_by_inval_name(self):
        with self.assertRaises(ValueError):
            self.nl._ref_to_port('X10')

    def test_port_by_obj(self):
        self.assertIs(self.nl._ref_to_port(self.ports[0]), self.ports[0])

    def test_port_by_inval_obj(self):
        with self.assertRaises(ValueError):
            self.nl._ref_to_port(self.ports2[0])

    def test_port_by_inval_ref(self):
        with self.assertRaises(TypeError):
            self.nl._ref_to_port(None)


class TestRename(unittest.TestCase):
    def setUp(self):
        self.nl = NetList()
        self.ports = list(self.nl.add_port(2) for i in range(2))
        self.ports[0][0].connect(self.ports[1][1])
        self.ports[0][1].connect(self.ports[1][0])
        self.nets = self.nl.nets

    def test_rename_net(self):
        net = self.nets['N0']
        self.assertEqual(net.name, 'N0')
        net.name = 'net1'
        self.assertEqual(net.name, 'net1')
        self.assertIn('net1', self.nl.nets)
        self.assertNotIn('N0', self.nl.nets)
        net = self.ports[1][1].get_net()
        self.assertEqual(net.name, 'net1')

    def test_rename_net_same_name(self):
        net = self.nets['N0']
        self.assertEqual(net.name, 'N0')
        net.name = 'N0'
        self.assertEqual(net.name, 'N0')
        self.assertIn('N0', self.nl.nets)

    def test_rename_net_duplicate(self):
        net = self.nets['N0']
        self.assertEqual(net.name, 'N0')
        dup_name = 'N1'
        self.assertIn(dup_name, self.nl.nets)
        with self.assertRaises(ValueError):
            net.name = dup_name

    def test_rename_port(self):
        port = self.ports[0]
        self.assertEqual(port.name, 'X0')
        port.name = 'Z0'
        self.assertEqual(port.name, 'Z0')
        self.assertIn('Z0', self.nl.ports)
        self.assertNotIn('X0', self.nl.ports)

    def test_rename_port_same_name(self):
        port = self.ports[0]
        self.assertEqual(port.name, 'X0')
        port.name = 'X0'
        self.assertEqual(port.name, 'X0')
        self.assertIn('X0', self.nl.ports)

    def test_rename_port_duplicate(self):
        port = self.ports[0]
        self.assertEqual(port.name, 'X0')
        dup_name = 'X1'
        self.assertIn(dup_name, self.nl.ports)
        with self.assertRaises(ValueError):
            port.name = dup_name


class TestConnectivity(unittest.TestCase):
    def setUp(self):
        self.nl = NetList()
        self.ports = list(self.nl.add_port(3) for i in range(4))
        self.nets = list(self.nl.add_net() for i in range(3))
        self.nl.connect(self.nets[0],
                self.ports[0][0],
                self.ports[1][0],
                self.ports[2][0])
        self.nl.connect(self.nets[1],
                self.ports[0][1],
                self.ports[1][1])
        self.nl.connect(self.nets[2],
                self.ports[1][2],
                self.ports[2][2])

    def test_connect_with_itself(self):
        num_nets = len(self.nl.nets)
        self.ports[0][0].connect(self.ports[1][0])
        self.assertEqual(num_nets, len(self.nl.nets))
        for port_i in range(3):
            self.assertTrue(self.nets[0].is_connected_with(self.ports[port_i][0]))

    def test_connected(self):
        for port1 in self.ports[:3]:
            for port2 in self.ports[:3]:
                self.assertTrue(port1[0].is_connected_with(port2[0]))
        self.assertTrue(self.ports[0][1].is_connected_with(self.ports[1][1]))
        self.assertTrue(self.ports[1][2].is_connected_with(self.ports[2][2]))

    def test_not_connected(self):
        for port in self.ports:
            if port is self.ports[3]:
                continue
            for pin_no in range(3):
                self.assertFalse(self.ports[3][0].is_connected_with(port[pin_no]))
        for port in self.ports:
            for pin_no in range(1, 3):
                self.assertFalse(self.nets[0].is_connected_with(port[pin_no]))

    def test_net_presence(self):
        self.assertTrue(self.ports[0][0].has_net())
        self.assertFalse(self.ports[2][1].has_net())


class TestJoining(unittest.TestCase):
    def setUp(self):
        self.nl = NetList()
        ports = list(self.nl.add_port(3) for i in range(3))
        ports[0][0].connect(ports[1][0])
        ports[1][1].connect(ports[2][1])
        self.ports = ports

    def test_setup(self):
        ports = self.ports
        self.assertTrue(ports[0][0].is_connected_with(ports[1][0]))
        self.assertTrue(ports[1][1].is_connected_with(ports[2][1]))
        self.assertFalse(ports[1][0].is_connected_with(ports[1][1]))
        self.assertFalse(ports[0][0].is_connected_with(ports[1][1]))
        self.assertEqual(len(self.nl.nets), 2)

    def test_joined(self):
        ports = self.ports
        ports[1][0].connect(ports[1][1])
        conntable = (
            ((0,0), (1,0)),
            ((1,0), (1,1)),
            ((1,1), (2,1)),
            ((0,0), (1,1)),
            ((1,0), (2,1)),
        )
        for p1, p2 in conntable:
            self.assertTrue(ports[p1[0]][p1[1]].is_connected_with(ports[p2[0]][p2[1]]))

    def test_not_joined(self):
        ports = self.ports
        ports[1][0].connect(ports[1][1])
        for port1 in self.ports:
            for port2 in self.ports:
                for pin_no in range(2):
                    self.assertFalse(port1[2].is_connected_with(port2[pin_no]))
        conntable = ((0,0), (1,0), (1,1), (2,1))
        for p in conntable:
            self.assertFalse(ports[p[0]][p[1]].is_connected_with(ports[2][0]))
            self.assertFalse(ports[p[0]][p[1]].is_connected_with(ports[0][1]))

    def test_deleted_old_net(self):
        ports = self.ports
        ports[1][0].connect(ports[1][1])
        self.assertEqual(len(self.nl.nets), 1)

    def test_no_new_net_on_dummy_connect(self):
        ports = self.ports
        ports[0][1].connect()
        self.assertEqual(len(self.nl.nets), 2)

    def test_no_new_net_on_self_connect(self):
        ports = self.ports
        ports[0][1].connect(ports[0][1])
        self.assertEqual(len(self.nl.nets), 2)


class TestFile(unittest.TestCase):
    def setUp(self):
        nl = NetList()
        ports = list(nl.add_port(i + 1, 'XX%d' % i) for i in range(3))
        self.orig_config = {'type': 'cool'}
        ports[0].config = self.orig_config
        ports[0]['1'].connect(ports[1]['1'])
        ports[1]['2'].connect(ports[2]['2'])
        ports[0]['1'].name = 'p0'
        ports[0]['1'].get_net().name = 'net0'

        json_io = StringIO()
        nl.save(json_io)
        self.orig_nl = nl

        json_io = StringIO(json_io.getvalue())
        nl = NetList()
        nl.load(json_io)
        self.nl = nl

    def test_port_names(self):
        orig_port_names = list(self.orig_nl.ports.keys())
        new_port_names = list(self.nl.ports.keys())
        self.assertEqual(orig_port_names, new_port_names)

    def test_port_pins(self):
        nl = self.nl
        for num_pins, port in enumerate(nl.ports.values(), 1):
            self.assertEqual(port.num_pins(), num_pins)
        self.assertEqual(nl.ports['XX0']['1'].name, 'p0')
        self.assertEqual(nl.ports['XX1']['1'].name, '1')

    def test_port_config(self):
        nl = self.nl
        self.assertEqual(nl.ports['XX0'].config, self.orig_config)

    def test_connections(self):
        nl = self.nl
        self.assertTrue(nl.is_connected('XX0.1', 'XX1.1'))
        self.assertTrue(nl.is_connected('XX1.2', 'XX2.2'))
        self.assertFalse(nl.is_connected('XX0.1', 'XX1.2'))

    def test_net_names(self):
        orig_net_names = set(self.orig_nl.nets.keys())
        new_net_names = set(self.nl.nets.keys())
        self.assertEqual(orig_net_names, new_net_names)
