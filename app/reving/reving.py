from enum import IntEnum
from abc import ABC, abstractmethod
import serial
import time

from reving.proto import *


class CmdId(IntEnum):
    RESET       = 0x00
    PINCOUNT    = 0x01
    READ        = 0x02
    OUTPUT      = 0x03
    NEXT_OUT    = 0x04
    SET_ADDR    = 0x05


class PinMode(IntEnum):
    LOW         = 0
    HIGH        = 1
    INPUT       = 2


def _validate_addr(addr, allow_broadcast=False):
    if type(addr) is not int:
        raise TypeError('invalid address type')
    elif addr not in range(int(not allow_broadcast), 256):
        raise ValueError('invalid address')


class Port():
    def __init__(self, bus, addr):
        self.bus = bus
        self.addr = addr
        self._num_pins = None
        self.rewind()

    def reset(self):
        self.bus.send(self.addr, Command(CmdId.RESET))

    def num_pins(self):
        if self._num_pins is None:
            self._num_pins = self.bus.request(self.addr, Command(CmdId.PINCOUNT)).args[0]
        return self._num_pins

    def read(self):
        bib = self.bus.request(self.addr, Command(CmdId.READ)).args
        return list(bool(bib[bit // 7] & (1 << (bit % 7))) for bit in range(self.num_pins()))

    def _validate_pin(self, pin):
        if type(pin) is not int:
            raise TypeError('invalid pin type')
        if pin not in range(self.num_pins()):
            raise ValueError('invalid pin')

    def output(self, pin, mode=False):
        self._validate_pin(pin)

        if mode is True:
            cmd = Command(CmdId.OUTPUT, pin, PinMode.HIGH)
        elif mode is False:
            cmd = Command(CmdId.OUTPUT, pin)
        elif mode is None:
            cmd = Command(CmdId.OUTPUT, pin, PinMode.INPUT)
        else:
            raise TypeError('invalid mode type')

        self.bus.send(self.addr, cmd)

    def rewind(self):
        self.iter = 0

    def try_next(self, pin=None):
        if pin is not None:
            self._validate_pin(pin)
            self.iter = pin
        elif self.iter >= self.num_pins():
            raise RuntimeError('next called too many times')
        self.bus.send(self.addr, Command(CmdId.NEXT_OUT, self.iter))
        self.iter += 1

    def set_addr(self, new_addr):
        _validate_addr(new_addr)
        self.bus._set_port_address(self, self.addr, new_addr)
        try:
            self.bus.send(self.addr, Command(CmdId.SET_ADDR, new_addr))
        except:
            self.bus._set_port_address(self, new_addr, self.addr)
            raise
        time.sleep(0.1)
        self.addr = new_addr


class BusError(Exception):
    pass


class NoDevice(BusError):
    def __init__(self, addr=None):
        msg = 'device '
        if addr is not None:
            msg += str(addr) + ' '
        msg += 'not responding'
        BusError.__init__(self, msg)


class BusBase(ABC):
    def __init__(self):
        self.ports = {}

    @abstractmethod
    def send(self, addr, cmd):
        pass

    @abstractmethod
    def recv(self):
        pass

    def request(self, addr, cmd):
        self.send(addr, cmd)
        try:
            return self.recv()
        except NoDevice:
            raise NoDevice(addr)

    def _probe_port(self, addr):
        port = Port(self, addr)
        if addr != 0:
            port.num_pins() # cache num_pins and make sure that port exists
        return port

    def get_port(self, addr, allow_broadcast=False):
        _validate_addr(addr, allow_broadcast)
        if addr not in self.ports:
            self.ports[addr] = self._probe_port(addr)
        return self.ports[addr]

    def _set_port_address(self, port, old_addr, new_addr):
        if old_addr == new_addr:
            return
        if new_addr in self.ports:
            raise BusError('address already in use')
        if old_addr in self.ports:
            del self.ports[old_addr]
        self.ports[new_addr] = port

    def reset(self):
        """reset all ports"""
        bport = Port(self, 0)
        bport.reset()


class Bus(BusBase):
    def __init__(self, tty=None):
        super().__init__()
        self.s = None
        if tty is not None:
            self.connect(tty)

    def connect(self, tty):
        try:
            self.s = serial.Serial(tty, 9600, timeout=0.1)
        except IOError:
            raise BusError('can\'t open serial port')

    def send(self, addr, cmd):
        if not isinstance(cmd, Command):
            raise TypeError('cmd must be a Command')
        if self.s is None:
            raise BusError('not connected')
        msg = cmd.compile(addr)
        self.s.write(msg)
        echo = self.s.read(len(msg))
        if echo != msg:
            raise BusError('bus conflict')

    def recv(self):
        b = self.s.read(1)
        if b == b'':
            raise NoDevice()
        b = b[0]
        nargs = b & 7
        args = self.s.read(nargs)
        if len(args) != nargs:
            raise BusError('received incomplete data')
        return Response(b >> 3, *args)
