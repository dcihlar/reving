class Packet():
    def __init__(self, cmd=0, *args):
        self.cmd = cmd
        self.args = list(args)
        self._validate_cmd()
        self._validate_args()

    def compile(self):
        self._validate_cmd()
        self._validate_args()
        return bytearray([
                (self.cmd << 3) | len(self.args)
            ] + self.args)

    def _validate_cmd(self):
        if self.cmd not in range(0, 16):
            raise ValueError('invalid command')

    def _validate_args(self):
        if len(self.args) > 8:
            raise ValueError('too many args')
        if any(map(lambda a: a not in range(0, 128), self.args)):
            raise ValueError('invalid argument')

    def __eq__(self, other):
        return self.cmd == other.cmd and self.args == other.args

    def __repr__(self):
        return '<%s: %x %s>' % (type(self).__name__, self.cmd, ' '.join('%02x' % a for a in self.args))


class Command(Packet):
    def compile(self, addr):
        if addr not in range(0, 128):
            raise ValueError('invalid address')
        return bytearray([(0x80 | addr)]) + super().compile()


class Response(Packet):
    pass
