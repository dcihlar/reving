from collections import defaultdict
from reving.netlist import NetList, Port, Pin
from reving.reving import NoDevice
from reving.reving import Port as HwPort


class UberNetListError(Exception):
    pass


class UberPort():
    def __init__(self, hwport, nlport):
        if not isinstance(hwport, HwPort):
            raise TypeError('invalid hardware port')
        if not isinstance(nlport, Port):
            raise TypeError('invalid netlist port')
        if hwport.num_pins() != nlport.num_pins():
            raise UberNetListError('hardware doesn\'t match the netlist')
        self.hwport = hwport
        self.nlport = nlport

    @property
    def addr(self):
        return self.hwport.addr

    def num_pins(self):
        return self.nlport.num_pins()

    def __getitem__(self, pin):
        return self.nlport[pin]

    def get_free_pins(self):
        return [i for i in range(self.nlport.num_pins()) if not self.nlport[i].has_net()]

    def get_active_pins(self):
        states = self.hwport.read()
        return [self.nlport[i] for i, nact in enumerate(states) if not nact]

    def reset(self):
        self.hwport.reset()

    def rewind(self):
        self.hwport.rewind()

    def try_next(self, pin_no=None):
        self.hwport.try_next(pin_no)


class UberNetList(NetList):
    def __init__(self, bus):
        super().__init__()
        self.bus = bus
        self.bus.reset()
        self.diode_id = 1

    def _port2u(self, ref):
        nlport = self._ref_to_port(ref)
        try:
            hwport = self.bus.get_port(nlport.config['addr'])
            return UberPort(hwport, nlport)
        except NoDevice as e:
            raise UberNetListError(str(e))

    def get_uports(self):
        return [self._port2u(port) for port in self.ports.values() if 'addr' in port.config]

    def load(self, *args, **kwargs):
        super().load(*args, **kwargs)
        addr_refs = set()
        for uport in self.get_uports():
            if uport.addr in addr_refs:
                raise UberNetListError('duplicate addresses in the netlist')
            addr_refs.add(uport.addr)

    def auto_init(self):
        for i in range(1, 128):
            try:
                hwport = self.bus.get_port(i)
                port = self.add_port(hwport.num_pins(), 'X%d' % i)
                port.config['addr'] = i
            except NoDevice:
                pass

    @staticmethod
    def _scan_pin(uports, start_pin):
        fixed_pin = uports[0][start_pin]
        for uport in uports:
            fixed_pin.connect(*uport.get_active_pins())

    def scan(self):
        # this algorihm only looks at unconnected pins
        uports = self.get_uports()
        for i, uport in enumerate(uports):
            for pin_no in uport.get_free_pins():
                uport.try_next(pin_no)
                self._scan_pin(uports[i:], pin_no)
            uport.reset()

    @staticmethod
    def _find_connections(uports):
        for uport in uports:
            yield from uport.get_active_pins()

    def _alloc_diode(self):
        while 'D%d' % self.diode_id in self.ports:
            self.diode_id += 1
        diode = self.add_port(2, 'D%d' % self.diode_id)
        self.diode_id += 1
        diode.config['component'] = 'diode'
        diode.pin_names[0] = 'A'
        diode.pin_names[1] = 'K'
        return diode

    @staticmethod
    def _find_same_net_pin(pin, pins, default=None):
        if pin in pins:
            return pin
        if pin.has_net():
            for pin2 in pins:
                if pin2.get_net() is pin.get_net():
                    return pin2
        return default

    def dscan(self):
        # this algorithm tests all combinations, detects diodes and performs connections
        connections = {}
        uports = self.get_uports()
        # make a full scan - this can take a while
        for uport in uports:
            for pin_no in range(uport.num_pins()):
                uport.try_next(pin_no)
                curpin = uport[pin_no]
                connections[curpin] = list(self._find_connections(uports))
                connections[curpin].remove(curpin)
            uport.reset()
        # connect direct connections first
        # also remove them from connections dict
        for pin, conns in connections.items():
            for pin2 in conns[:]:
                if pin2 in connections and pin in connections[pin2]:
                    pin.connect(pin2)
                    conns.remove(pin2)
                    connections[pin2].remove(pin)
        # only diodes are left in connections
        # elliminate duplicate nets
        diodeconns = defaultdict(list)
        for pin, conns in connections.items():
            key = self._find_same_net_pin(pin, diodeconns.keys(), pin)
            uniqueconns = diodeconns[key]
            for pin2 in conns:
                if self._find_same_net_pin(pin2, uniqueconns) is None:
                    uniqueconns.append(pin2)
        # insert diodes
        for pinnet, conns in diodeconns.items():
            for pinnet2 in conns:
                diode = self._alloc_diode()
                diode['K'].connect(pinnet)
                diode['A'].connect(pinnet2)

    def get_active_pins(self):
        act_pins = []
        for uport in self.get_uports():
            act_pins += uport.get_active_pins()
        return act_pins
