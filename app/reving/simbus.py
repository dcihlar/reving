from reving.netlist import NetList
from reving.reving import BusBase, Response, NoDevice, CmdId, PinMode


class PinArray():
    def __init__(self, size):
        self.d = [True] * size

    def __len__(self):
        return len(self.d)

    def __getitem__(self, idx):
        return self.d[idx]

    def __setitem__(self, idx, val):
        self.d[idx] = bool(val)

    def to_bytes(self):
        d = self.d[:]
        d += [False] * (7 - len(d) % 7)
        return bytearray(
            sum(int(d[b+i]) << i for i in range(7))
                for b in range(0, len(d), 7))


class SimBus(BusBase):
    def __init__(self, netlist):
        if not isinstance(netlist, NetList):
            raise TypeError('expecting NetList')

        super().__init__()

        self.nl = netlist
        self._next_response = None
        self.zeros = set()

        self.nlport_by_addr = {}
        for port_name, nlport in self.nl.ports.items():
            if not 'addr' in nlport.config:
                continue
            self.nlport_by_addr[nlport.config['addr']] = nlport

    def _set_zero(self, nlport, pin_no, state=PinMode.LOW):
        pin = nlport[pin_no]
        if state == PinMode.LOW:
            self.zeros.add(pin)
        elif pin in self.zeros:
            self.zeros.remove(pin)

    def _reset_port(self, nlport):
        portpins = set(nlport[i] for i in range(nlport.num_pins()))
        self.zeros -= portpins

    def send(self, addr, cmd):
        if addr not in self.nlport_by_addr:
            self._next_response = None
            return
        nlport = self.nlport_by_addr[addr]
        if cmd.cmd == CmdId.PINCOUNT:
            self._next_response = Response(0, nlport.num_pins())
        elif cmd.cmd == CmdId.READ:
            pins = PinArray(nlport.num_pins())
            for zero_pin in self.zeros:
                for pin_no in range(nlport.num_pins()):
                    pins[pin_no] &= not nlport[pin_no].is_connected_with(zero_pin)
                if zero_pin.port is nlport:
                    pins[zero_pin.pin_no] = False
            self._next_response = Response(0, *pins.to_bytes())
        elif cmd.cmd == CmdId.OUTPUT:
            self._set_zero(nlport, *cmd.args)
        elif cmd.cmd == CmdId.NEXT_OUT:
            self._reset_port(nlport)
            self._set_zero(nlport, *cmd.args)
        elif cmd.cmd == CmdId.RESET:
            self._reset_port(nlport)
        else:
            raise RuntimeError("don't know how to handle %s" % repr(cmd.cmd))

    def recv(self):
        resp = self._next_response
        self._next_response = None
        if resp is None:
            raise NoDevice
        return resp
