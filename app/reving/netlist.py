import string
import json


class Port():
    def __init__(self, netlist, name, num_pins):
        if type(num_pins) is not int:
            raise TypeError('num_pins must be int')
        if num_pins < 0:
            raise ValueError('invalid number of pins')
        self.netlist = netlist
        self._name = name
        self._num_pins = num_pins
        self.pins = [None] * num_pins
        self.pin_names = list(str(i+1) for i in range(num_pins))
        self.config = {}

    def get_name(self):
        return self._name

    def set_name(self, new_name):
        self.netlist.rename_port(self, new_name)

    name = property(get_name, set_name)

    def num_pins(self):
        return self._num_pins

    def get_pin_net(self, pin_no):
        self._validate_pin_no(pin_no)
        return self.pins[pin_no]

    def pin_has_net(self, pin_no):
        return self.get_pin_net(pin_no) is not None

    def __getitem__(self, pin_ref):
        pin_no = pin_ref
        if type(pin_ref) is str:
            pin_no = int(pin_ref)-1 if pin_ref.isdigit() else self.pin_names.index(pin_ref)
        self._validate_pin_no(pin_no)
        return Pin(self, pin_no)

    def _validate_pin_no(self, pin_no):
        if type(pin_no) is not int:
            raise TypeError('pin_no must be int')
        if pin_no not in range(self.num_pins()):
            raise ValueError('invalid pin number')


class Pin():
    def __init__(self, port, pin_no):
        if not isinstance(port, Port):
            raise TypeError('port must be a Port instance')
        port._validate_pin_no(pin_no)
        self.port = port
        self.pin_no = pin_no

    def get_net(self):
        return self.port.get_pin_net(self.pin_no)

    def has_net(self):
        return self.port.pin_has_net(self.pin_no)

    def connect(self, *refs):
        self.port.netlist.connect(self, *refs)

    def is_connected_with(self, ref):
        return self.port.netlist.is_connected(self, ref)

    @staticmethod
    def validate_name(name):
        if type(name) is not str:
            raise TypeError('pin name must be str')
        if not name or \
           set(name) & set(string.whitespace) or \
           name[0] in string.digits:
            raise ValueError('invalid pin name')
        return name

    def get_name(self):
        return self.port.pin_names[self.pin_no]

    def set_name(self, new_name):
        self.port.pin_names[self.pin_no] = Pin.validate_name(new_name)

    name = property(get_name, set_name)

    def __eq__(self, other):
        return self.port is other.port and self.pin_no == other.pin_no

    def __hash__(self):
        return hash(self.port.name + str(self.pin_no))

    @property
    def desc(self):
        name = self.name
        if name.isdigit():
            return '%s.%s' % (self.port.name, name)
        return '%s.%d(%s)' % (self.port.name, self.pin_no+1, name)

    def __repr__(self):
        return  '<Pin %s>' % self.desc


class Net():
    def __init__(self, netlist, name):
        self.netlist = netlist
        self._name = name
        self.junctions = []

    def set_name(self, new_name):
        self.netlist.rename_net(self, new_name)

    def get_name(self):
        return self._name

    name = property(get_name, set_name)

    def connect(self, *refs):
        self.netlist.connect(self, *refs)

    def is_connected_with(self, ref):
        return self.netlist.is_connected(self, ref)

    def __eq__(self, other):
        return self is other

    def __hash__(self):
        return hash(self._name)

    def __repr__(self):
        return '<Net %s>' % self._name


class NetList():
    def __init__(self):
        self.clear()

    def clear(self):
        self.ports = {}
        self.nets = {}

    def _validate_port(self, port):
        if not isinstance(port, Port):
            raise TypeError('port must be a Port instance')
        if port not in self.ports.values():
            raise RuntimeError('port is not mine')

    def _new_name(self, prefix, container):
        for i in range(len(container) + 1):
            new_name = prefix + str(i)
            if new_name not in container:
                return new_name
        raise RuntimeError('internal error - can\'t generate a new name')

    def add_port(self, num_pins, name=None):
        if name is None:
            name = self._new_name('X', self.ports)
        elif name in self.ports:
            raise ValueError('port name already in use')
        port = Port(self, name, num_pins)
        self.ports[name] = port
        return port

    def rename_port(self, ref, new_name):
        port = self._ref_to_port(ref)
        if port._name == new_name:
            return
        if new_name in self.ports:
            raise ValueError('port name already in use')
        del self.ports[port._name]
        port._name = new_name
        self.ports[port._name] = port

    def add_net(self, net_name=None):
        if net_name is None:
            net_name = self._new_name('N', self.nets)
        elif net_name in self.nets:
            raise ValueError('net name already in use')
        net = Net(self, net_name)
        self.nets[net_name] = net
        return net

    def rename_net(self, ref, new_name):
        net = self._ref_to_net(ref)
        if net._name == new_name:
            return
        if new_name in self.nets:
            raise ValueError('net name already in use')
        del self.nets[net._name]
        net._name = new_name
        self.nets[net._name] = net

    def _ref_to_port(self, ref):
        if type(ref) is str:
            if ref not in self.ports:
                raise ValueError('unknown port "%s"' % ref)
            port = self.ports[ref]
        elif isinstance(ref, Port):
            port = ref
            if port not in self.ports.values():
                raise ValueError('not my port')
        else:
            raise TypeError('can\'t deduce ref type "%s"' % type(ref).__name__)
        return port

    def _ref_to_net(self, ref, alloc_net=False):
        # this only converts string to ref
        if type(ref) is str:
            if '.' in ref:
                port_name, pin_name = ref.split('.')
                port = self._ref_to_port(port_name)
                if pin_name.isdigit():
                    ref = port[int(pin_name)-1]
                else:
                    ref = port[port.pin_names.index(pin_name)]
            else:
                if ref not in self.nets:
                    raise ValueError('unknown net "%s"' % ref)
                ref = self.nets[ref]

        if isinstance(ref, Pin):
            pin = ref
            if pin.port not in self.ports.values():
                raise ValueError('not my pin (port)')
            net = pin.get_net()
            if net is None and alloc_net:
                net = self.add_net()
                net.junctions.append(pin)
                pin.port.pins[pin.pin_no] = net
        elif isinstance(ref, Net):
            net = ref
            if net not in self.nets.values():
                raise ValueError('not my net')
        else:
            raise TypeError('can\'t deduce ref type "%s"' % type(ref).__name__)

        return net

    def delete_net(self, ref):
        net = self._ref_to_net(ref)
        if net is None:
            return
        del self.nets[net.name]
        for pin in net.junctions:
            pin.port.pins[pin.pin_no] = None

    def connect(self, ref, *refs):
        net1 = self._ref_to_net(ref)
        net1_is_new = False
        if net1 is None:
            net1 = self._ref_to_net(ref, True)
            net1_is_new = True

        for ref in refs:
            net2 = self._ref_to_net(ref, True)
            if net1 is net2:
                continue
            del self.nets[net2.name]
            for pin in net2.junctions:
                pin.port.pins[pin.pin_no] = net1
            net1.junctions += net2.junctions
        if net1_is_new and len(net1.junctions) < 2:
            self.delete_net(net1)

    def is_connected(self, ref1, ref2):
        net1 = self._ref_to_net(ref1)
        net2 = self._ref_to_net(ref2)
        return net1 is net2 and net1 is not None

    def save(self, fobj):
        def portpin2jsoe(pin_name_with_net):
            pin_name, net = pin_name_with_net
            return pin_name, net.name if net is not None else None

        def port2jso(port):
            return port.name, {
                'config': port.config,
                'pins': dict(map(portpin2jsoe, zip(port.pin_names, port.pins))),
            }
        jsobj = dict(map(port2jso, self.ports.values()))
        json.dump(jsobj, fobj, ensure_ascii=False, indent=2)

    def load(self, fobj):
        jsobj = json.load(fobj)
        self.clear()
        for port_name, port_jso in jsobj.items():
            pins_jso = port_jso['pins']
            port = self.add_port(len(pins_jso), name=port_name)
            port.config = port_jso.get('config', {})
            for pin_no, pin_name in enumerate(pins_jso.keys()):
                if not pin_name.isdigit():
                    port[pin_no].name = pin_name
                elif pin_name != str(pin_no+1):
                    raise ValueError('invalid naming scheme')
                net_name = pins_jso[pin_name]
                if net_name is None:
                    continue
                if net_name in self.nets:
                    net = self.nets[net_name]
                else:
                    net = self.add_net(net_name)
                self.connect(net, port[pin_no])
