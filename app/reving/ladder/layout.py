from collections import defaultdict


class Span():
    def __init__(self, pos1, pos2 = None):
        if isinstance(pos1, Span) or type(pos1) is tuple:
            if pos2 is not None:
                raise ValueError('for this case for2 must be None')
            pos1, pos2 = pos1
        elif pos2 is None:
            pos2 = pos1
        self.pos = (pos1, pos2)
        self.isrow = isinstance(pos1, Row)
        self._validate(pos2)

    def on_endpoint(self, pos):
        self._validate(pos)
        return pos is self.pos[0] or pos is self.pos[1]

    def in_range(self, pos):
        self._validate(pos)
        pos_ofs_range = self.offsets()
        return pos.ofs() in range(pos_ofs_range[0], pos_ofs_range[1] + 1)

    def overlapping(self, other):
        my_offsets = self.offsets()
        other_offsets = other.offsets()
        return max(my_offsets[0], other_offsets[0]) <= min(my_offsets[1], other_offsets[1])

    def offsets(self):
        return sorted(pos.ofs() for pos in self.pos)

    def ofs(self):
        if self.pos[0] is self.pos[1]:
            return self.pos.ofs()
        raise RuntimeError('can\'t get ofs of a range')

    def __iter__(self):
        return iter(self.pos)

    def __getitem__(self, index):
        return self.pos[index]

    def _validate(self, pos):
        if self.isrow and not isinstance(pos, Row):
            raise TypeError('this span only works with rows')
        elif not self.isrow and not isinstance(pos, Column):
            raise TypeError('this span only works with columns')


class Container():
    def __init__(self, diag):
        self.diag = diag
        self.items = []

    def set(self, pos, item):
        item.diag = self.diag
        self.items.append((Span(pos), item))

    def get_items(self, itemtype=None):
        if itemtype is None:
            return [item for pos, item in self.items]
        return [item for pos, item in self.items if isinstance(item, itemtype)]

    def _validate_col(self, col):
        if not isinstance(col, Column):
            raise TypeError('expecting Column')

    def _validate_row(self, row):
        if not isinstance(row, Row):
            raise TypeError('expecting Row')

    def find_item_on_pos(self, pos, default = None):
        self._validate_orthogonal(pos)
        for item_pos, item in self.items:
            if item_pos.in_range(pos):
                return item
        return default

    def has_items_in_range(self, span):
        self._validate_orthogonal(span[0])
        for item_pos, item in self.items:
            if item_pos.overlapping(span):
                return True
        return False

    def __repr__(self):
        return '<%s @%d>' % (type(self).__name__, self.ofs())

class Row(Container):
    def __init__(self, diag, rung):
        super().__init__(diag)
        self.rung = rung

    def set(self, pos, elem):
        super().set(pos, elem)
        elem.row = self
        elem.col = Span(pos) if type(pos) is tuple else pos

    def rel_ofs(self):
        return self.rung.rows.index(self)

    def ofs(self):
        return self.rung.first_ofs() + self.rel_ofs()

    def below(self):
        belowofs = self.rel_ofs() + 1
        if belowofs >= len(self.rung.rows):
            return None
        return self.rung.rows[belowofs]

    find_item_on_col = Container.find_item_on_pos
    _validate_orthogonal = Container._validate_col
    _validate = Container._validate_row

class NodeRow(Row):
    def insert_above_wire(self):
        return self.rung.insert_row(self, WireRow)

    def insert_below_node(self):
        below = self.below()
        if below is None:
            return self.rung.add_row(NodeRow)
        else:
            return self.rung.insert_row(below, NodeRow)

class WireRow(Row):
    pass


class Column(Container):
    def set(self, pos, elem):
        super().set(pos, elem)
        elem.col = self
        elem.row = Span(pos) if type(pos) is tuple else pos

    def ofs(self):
        return self.diag.cols.index(self)

    def left(self):
        leftofs = self.ofs() - 1
        if leftofs < 0:
            return None
        return self.diag.cols[leftofs]

    def right(self):
        rightofs = self.ofs() + 1
        if rightofs >= len(self.diag.cols):
            return None
        return self.diag.cols[rightofs]

    def left_node(self):
        left = self.left()
        while not isinstance(left, NodeColumn):
            if left is None:
                return self.insert_left_node()
            left = left.left()
        return left

    def right_node(self, N=1):
        right = self
        for i in range(N):
            right = right.right()
            while not isinstance(right, NodeColumn):
                if right is None:
                    right = self.add_right_node()
                    break
                right = right.right()
        return right

    def left_wire(self):
        left = self.left()
        if isinstance(left, WireColumn):
            return left
        return self.insert_left_wire()

    def right_wire(self):
        right = self.right()
        if isinstance(right, WireColumn):
            return right
        return self.insert_right_wire()

    def insert_left_node(self):
        return self.diag.insert_col(self, NodeColumn)

    def add_right_node(self):
        return self.diag.add_node_col()

    def insert_right_node(self):
        return self.diag.insert_col(self.right(), NodeColumn)

    def insert_left_wire(self):
        return self.diag.insert_col(self, WireColumn)

    def insert_right_wire(self):
        return self.diag.insert_col(self.right(), WireColumn)

    find_item_on_row = Container.find_item_on_pos
    _validate_orthogonal = Container._validate_row
    _validate = Container._validate_col

class NodeColumn(Column):
    pass

class WireColumn(Column):
    pass

class SupplyColumn(Column):
    def __init__(self, diag, name):
        super().__init__(diag)
        self.name = name

class SourceColumn(SupplyColumn):
    pass

class SinkColumn(SupplyColumn):
    pass


class Draweable():
    def __init__(self):
        self.row = None
        self.col = None
        self.diag = None

class Node(Draweable):
    def __init__(self, name):
        super().__init__()
        self.name = name
        self.style = 'undefined'
        self.nets = [None, None]
        self.orientation = None
        self.padding = 0
        self.placed = False

    def set_placed(self):
        if self.placed:
            raise RuntimeError('already placed')
        self.placed = True

    def is_placed(self):
        return self.placed

    def set_net(self, pos, net_name):
        if pos not in range(0, 2):
            raise ValueError('invalid net position')
        if type(net_name) is not str:
            raise TypeError('invalid net')
        if self.nets[pos] is not None and self.nets[pos] != net_name:
            raise RuntimeError('trying to assign different nets to a same port')
        self.nets[pos] = net_name

    def is_flipped(self):
        if self.nets[0] is None and self.nets[1] is not None:
            # this is for ports
            # but consequentially it will flip two-port components connected only on the right side
            return True
        if self.orientation is not None and self.orientation != self.nets:
            if list(reversed(self.orientation)) != self.nets:
                raise RuntimeError('orientation has different nets')
            return True
        return False

    def connected_at(self, pos):
        return self.nets[pos] is not None

    def set_padding(self, padding):
        self.padding = padding

    def __repr__(self):
        return '<Node %s>' % self.name

class Wire(Draweable):
    def __init__(self, start='middle', end='middle'):
        super().__init__()
        self.start = start
        self.end = end


class LayoutError(Exception):
    def __init__(self, msg):
        super().__init__(msg)


class NodeLocator():
    def __init__(self, nodes):
        self.nodes = nodes

    def by_x(self):
        byx = defaultdict(list)
        for node in sorted(self.nodes, key=lambda node: node.col.ofs()):
            byx[node.col].append(node)
        return byx

    def by_y(self):
        byy = defaultdict(list)
        for node in sorted(self.nodes, key=lambda node: node.row.ofs()):
            byy[node.row].append(node)
        return byy

    def left(self):
        return next(iter(self.by_x().values()))

    def right(self):
        return next(iter(reversed(self.by_x().values())))

    def top(self):
        return next(iter(self.by_y().values()))

    def bottom(self):
        return next(iter(reversed((self.by_y().values()))))

    def top_row(self):
        return self.top()[0].row

    def bottom_row(self):
        return self.bottom()[0].row

    def on_row(self, row):
        return list(node for node in self.nodes if node.row is row)

    def on_col(self, col):
        return list(node for node in self.nodes if node.col is col)

    def __repr__(self):
        return '<NodeLocator %s>' % repr(self.nodes)


class Rung():
    def __init__(self, diag):
        self.diag = diag
        self.rows = []

    def num_rows(self):
        return len(self.rows)

    def first_ofs(self):
        my_ofs = self.diag.rungs.index(self)
        return sum(map(Rung.num_rows, self.diag.rungs[:my_ofs]))

    def add_row(self, rowtype):
        row = rowtype(self.diag, self)
        self.rows.append(row)
        return row

    def insert_row(self, rowpos, rowtype):
        row = rowtype(self.diag, self)
        self.rows.insert(rowpos.rel_ofs(), row)
        return row

    def get_rows(self, rowtype=Row):
        return [row for row in self.rows if isinstance(row, rowtype)]

    def merge(self, other):
        if not isinstance(other, Rung):
            raise TypeError('other must be another Rung')
        if other is self:
            return  # nothing to do
        self.rows += other.rows
        for row in other.rows:
            row.rung = self
        self.diag.rungs.remove(other)


class Diagram():
    def __init__(self):
        self.cols = []
        self.rungs = []
        self.nodes = {}
        self.supplies = {}
        self.add_node_col()

    def _register_supply(self, name, supplytype):
        if name in self.supplies:
            raise KeyError('supply already exists')
        supply = supplytype(self, name)
        self.supplies[name] = supply
        return supply

    def add_source_col(self, name):
        col = self._register_supply(name, SourceColumn)
        self.cols.insert(0, col)
        return col

    def add_sink_col(self, name):
        col = self._register_supply(name, SinkColumn)
        self.cols.append(col)
        return col

    def add_node_col(self):
        col = NodeColumn(self)
        # insert before sink columns
        for ofs, coli in enumerate(self.cols):
            if isinstance(coli, SinkColumn):
                self.cols.insert(coli.ofs(), col)
                return col
        self.cols.append(col)
        return col

    def insert_col(self, colpos, coltype):
        col = coltype(self)
        self.cols.insert(colpos.ofs(), col)
        return col

    def get_cols(self, coltype=Column):
        return [col for col in self.cols if isinstance(col, coltype)]

    def get_rows(self, rowtype=Row):
        for rung in self.rungs:
            yield from rung.get_rows(rowtype)

    def get_rungs(self):
        return self.rungs

    def get_node(self, node_name):
        if isinstance(node_name, Node):
            return node_name
        if node_name in self.nodes:
            return self.nodes[node_name]
        node = Node(node_name)
        node.diag = self
        self.nodes[node_name] = node
        return node

    def get_first_node_col(self):
        return next(filter(lambda col: isinstance(col, NodeColumn), self.cols))

    def add_rung(self):
        rung = Rung(self)
        self.rungs.append(rung)
        return rung

    def _place_node(self, row, col, node):
        node.set_placed()
        row.set(col.right_node(node.padding), node)

    def insert_start_node(self, node_name, col=None):
        rung = self.add_rung()
        row = rung.add_row(NodeRow)
        if col is None:
            col = self.get_first_node_col()
        node = self.get_node(node_name)
        self._place_node(row, col, node)
        return node

    def connect_to_source(self, srcname, *nodes):
        supply = self.supplies[srcname]
        if not isinstance(supply, SourceColumn):
            raise ValueError('expecting source')
        for node in map(self.get_node, nodes):
            if node.is_placed():
                raise NotImplementedError('don\'t know how to connect placed nodes to source')
            self.insert_start_node(node)
            node.set_net(0, srcname)
            node.row.set((supply, node.col), Wire(end='node'))

    def connect_to_sink(self, snkname, *nodes):
        supply = self.supplies[snkname]
        if not isinstance(supply, SinkColumn):
            raise ValueError('expecting sink')
        for node in map(self.get_node, nodes):
            connect_at = 1
            if not node.is_placed():
                self.insert_start_node(node, supply.left_node())
            else:
                if node.connected_at(connect_at):
                    connect_at = 0
            node.set_net(connect_at, snkname)
            if connect_at == 1:
                node.row.set((node.col, supply), Wire(start='node'))
            else:
                wrow = node.row.insert_above_wire()
                wcol = self.add_vertical_wire(node.col.left_wire(), (wrow, node.row))
                wrow.set((wcol, supply), Wire())
                node.row.set((wcol, node.col), Wire(end='node'))

    def connect_to_supply(self, supply, *nodes):
        if supply not in self.supplies:
            raise ValueError('unrecognized supply name')
        if isinstance(self.supplies[supply], SourceColumn):
            self.connect_to_source(supply, *nodes)
        else:
            self.connect_to_sink(supply, *nodes)

    def add_vertical_wire(self, col, span):
        while col.has_items_in_range(Span(span)):
            col = col.right_wire()
        col.set(span, Wire())
        return col

    def connect_between_nodes(self, net_name, *nodes):
        if not nodes:
            return
        nodes = list(map(self.get_node, nodes))
        existing_nodes = list(filter(Node.is_placed, nodes))
        new_nodes = list(node for node in nodes if node not in existing_nodes)
        nodes = existing_nodes + new_nodes

        if not existing_nodes:
            raise NotImplementedError('don\'t know how to lay out orphaned nodes')

        # find top right existing node
        right = NodeLocator(NodeLocator(existing_nodes).right())
        topright = right.top()[0]

        # merge existing rungs
        for node in set(existing_nodes) - set([topright]):
            topright.row.rung.merge(node.row.rung)

        # insert new nodes next to topright
        col = topright.col.right_node()
        row = topright.row
        for node in new_nodes:
            self._place_node(row, col, node)
            next_row = row.below()
            if right.on_row(next_row):
                row = next_row
            elif node is not new_nodes[-1]:
                row = row.insert_below_node()

        # connect them
        toprow = NodeLocator(nodes).top_row()
        botrow = NodeLocator(nodes).bottom_row()
        col = topright.col.right_wire()
        if toprow is not botrow:
            col = self.add_vertical_wire(col, (toprow, botrow))
            for node in existing_nodes:
                node.set_net(1, net_name)
                node.row.set((node.col, col), Wire(start='node'))
            for node in new_nodes:
                node.set_net(0, net_name)
                node.row.set((col, node.col), Wire(end='node'))
        elif len(existing_nodes) == 1 and len(new_nodes) == 1:
            old_node = existing_nodes[0]
            new_node = new_nodes[0]
            old_node.row.set((old_node.col, new_node.col), Wire(start='node', end='node'))
            old_node.set_net(1, net_name)
            new_node.set_net(0, net_name)
        elif len(existing_nodes) == 1 and len(new_nodes) == 0:
            existing_nodes[0].set_net(1, net_name)
        else:
            RuntimeError('unexpected scenario')

    def connect(self, net_name, *nodes):
        if net_name in self.supplies:
            self.connect_to_supply(net_name, *nodes)
        else:
            self.connect_between_nodes(net_name, *nodes)
