from collections import defaultdict
import os.path
import cairo
import math
import io

from .layout import Row, Column, SupplyColumn, WireColumn, NodeColumn, WireRow, NodeRow, Node, Wire


NODE_SYM_DIM = 30
NODE_SYM_R = NODE_SYM_DIM / 2
NODE_SYM_Q = NODE_SYM_DIM / 4
NODE_CELL_WIDTH = 64
NODE_CELL_HEIGHT = 64
RUNG_SPACING = 32

def width_from_type(col):
    if isinstance(col, SupplyColumn):
        return 16
    elif isinstance(col, WireColumn):
        return 8
    elif isinstance(col, NodeColumn):
        return NODE_CELL_WIDTH
    else:
        raise TypeError('unknown column type')

def height_from_type(row):
    if isinstance(row, WireRow):
        return 8
    elif isinstance(row, NodeRow):
        return NODE_CELL_HEIGHT
    else:
        raise TypeError('unknown row type')


class Pos():
    def __init__(self, layout):
        # calculate dimensions
        self.lefts = {}
        self.rights = {}
        self.width = 0
        for col in layout.cols:
            self.lefts[col] = self.width
            self.width += width_from_type(col)
            self.rights[col] = self.width
        self.tops = {}
        self.bottoms = {}
        self.height = 0
        for rung in layout.get_rungs():
            for row in rung.get_rows():
                self.tops[row] = self.height
                self.height += height_from_type(row)
                self.bottoms[row] = self.height
            self.height += RUNG_SPACING

    def left(self, col):
        return self.lefts[col]
    def right(self, col):
        return self.rights[col]
    def top(self, row):
        return self.tops[row]
    def bottom(self, row):
        return self.bottoms[row]
    def center_one(self, rowcol):
        if isinstance(rowcol, Column):
            return (self.left(rowcol) + self.right(rowcol)) / 2
        elif isinstance(rowcol, Row):
            return (self.top(rowcol) + self.bottom(rowcol)) / 2
        else:
            raise TypeError('expecting row or col')
    def center(self, first, second=None):
        if second is None: return self.center_one(first)
        return self.center_one(first), self.center_one(second)


class RenderCairoNode():
    def __init__(self, ctx):
        self.ctx = ctx
        self.qpos = self._calc_text_pos('?')
        self.tonpos = self._calc_text_pos('TON')

    def _calc_text_pos(self, txt):
        ext = self.ctx.text_extents(txt)
        return (-ext.width/2, ext.height/2)

    def _draw_nets(self, symwidth=NODE_SYM_R):
        self.ctx.move_to(-NODE_CELL_WIDTH/2, 0)
        self.ctx.line_to(-symwidth, 0)
        self.ctx.move_to(symwidth, 0)
        self.ctx.line_to(NODE_CELL_WIDTH/2, 0)
        self.ctx.stroke()

    def draw_NO(self):
        self.ctx.move_to(-NODE_SYM_Q, -NODE_SYM_R)
        self.ctx.line_to(-NODE_SYM_Q, NODE_SYM_R)
        self.ctx.move_to(NODE_SYM_Q, -NODE_SYM_R)
        self.ctx.line_to(NODE_SYM_Q, NODE_SYM_R)
        self._draw_nets(NODE_SYM_Q)

    def draw_NC(self):
        self.draw_NO()
        self.ctx.move_to(NODE_SYM_R, -NODE_SYM_R)
        self.ctx.line_to(-NODE_SYM_R, NODE_SYM_R)
        self.ctx.stroke()

    def draw_Diode(self):
        self.ctx.move_to(-NODE_SYM_Q, NODE_SYM_R)
        self.ctx.line_to(-NODE_SYM_Q, -NODE_SYM_R)
        self.ctx.line_to(NODE_SYM_Q, 0)
        self.ctx.close_path()
        self.ctx.move_to(NODE_SYM_Q, -NODE_SYM_R)
        self.ctx.line_to(NODE_SYM_Q, NODE_SYM_R)
        self._draw_nets(NODE_SYM_Q)

    def draw_Coil(self):
        angle = 0.4 * math.pi
        self.ctx.new_sub_path()
        self.ctx.arc(0, 0, NODE_SYM_R, -angle, angle)
        self.ctx.new_sub_path()
        self.ctx.arc(0, 0, NODE_SYM_R, math.pi-angle, math.pi+angle)
        self._draw_nets()

    def draw_TON(self):
        self.draw_Coil()
        self.ctx.move_to(*self.tonpos)
        self.ctx.show_text('TON')

    def draw_Lamp(self):
        self.ctx.new_sub_path()
        self.ctx.arc(0, 0, NODE_SYM_R, 0, 2*math.pi)
        self._draw_nets()
        sq = NODE_SYM_R / math.sqrt(2)
        self.ctx.move_to(-sq, -sq)
        self.ctx.line_to(sq, sq)
        self.ctx.move_to(sq, -sq)
        self.ctx.line_to(-sq, sq)
        self.ctx.stroke()

    def _draw_rect(self):
        self.ctx.rectangle(-NODE_SYM_R, -NODE_SYM_Q, NODE_SYM_DIM, NODE_SYM_R)

    def draw_Fuse(self):
        self._draw_rect()
        self.ctx.move_to(-NODE_CELL_WIDTH/2, 0)
        self.ctx.line_to(NODE_CELL_WIDTH/2, 0)
        self.ctx.stroke()

    def draw_Port(self):
        self._draw_rect()
        self.ctx.move_to(-NODE_CELL_WIDTH/2, 0)
        self.ctx.line_to(-NODE_SYM_R, 0)
        self.ctx.stroke()

    def draw_Resistor(self):
        self._draw_rect()
        self._draw_nets()

    def draw_Jumper(self):
        self.ctx.new_sub_path()
        self.ctx.arc(0, 0, NODE_SYM_R, math.pi, 0)
        self._draw_nets()

    def draw_undefined(self):
        self.ctx.rectangle(-NODE_SYM_R, -NODE_SYM_R, NODE_SYM_DIM, NODE_SYM_DIM)
        self._draw_nets()
        self.ctx.move_to(*self.qpos)
        self.ctx.show_text('?')

    def draw(self, style, flip=False):
        method = 'draw_' + style
        if not hasattr(self, method):
            self.draw_undefined()
        else:
            if flip:
                self.ctx.save()
                self.flip()
            getattr(self, method)()
            if flip:
                self.ctx.restore()

    def flip(self):
        matrix = self.ctx.get_matrix()
        matrix.xx *= -1
        self.ctx.set_matrix(matrix)


class RenderCairo():
    def __init__(self, layout):
        self.layout = layout
        self.pos = Pos(layout)
        self.node_drawer = None
        self.wire_ends = defaultdict(int)
        self.fobj = None
        self.ctx = None
        self.surface = None

    def draw_dot(self, size):
        self.ctx.arc(0, 0, size, 0, 2*math.pi)
        self.ctx.fill()

    def draw_wire(self, dwbl, right, celldimstart, celldimend):
        if dwbl.start == 'middle':
            x1 = celldimstart / 2
        elif dwbl.start == 'node':
            x1 = celldimstart
        else:
            raise ValueError('unsupported wire start "%s"' % dwbl.start)
        if dwbl.end == 'middle':
            x2 = right - celldimend / 2
        elif dwbl.end == 'node':
            x2 = right - celldimend
        else:
            raise ValueError('unsupported wire end "%s"' % dwbl.end)
        self.ctx.move_to(x1, 0)
        self.ctx.line_to(x2, 0)
        self.ctx.stroke()

    def _finish(self):
        self.surface.finish()

    def alloc_surface(self, fobj):
        width = self.pos.width
        height = self.pos.height
        if width < 1 or height < 1:
            raise ValueError('nothing to draw')
        fname = fobj.name if isinstance(fobj, io.IOBase) else fobj
        _, ext = os.path.splitext(fname)
        ext = ext.lower()
        if ext == '.png':
            self.surface = cairo.ImageSurface(cairo.FORMAT_RGB24, width, height)
            self._finish = lambda: self.surface.write_to_png(fobj)
        elif ext == '.svg':
            self.surface = cairo.SVGSurface(fobj, width, height)
        elif ext == '.pdf':
            self.surface = cairo.PDFSurface(fobj, width, height)
        else:
            raise ValueError('unknown output file format')

    def render(self, fobj):
        # init cairo
        self.alloc_surface(fobj)
        #self.surface = cairo.ImageSurface(cairo.FORMAT_RGB24, self.pos.width, self.pos.height)
        ctx = cairo.Context(self.surface)
        self.node_drawer = RenderCairoNode(ctx)
        self.ctx = ctx
        # white background
        ctx.set_source_rgb(1, 1, 1)
        ctx.paint()
        # draw supply wires
        ctx.set_source_rgb(0, 0, 0)
        ctx.set_line_width(3)
        for supplycol in self.layout.get_cols(SupplyColumn):
            x = self.pos.center(supplycol)
            ctx.move_to(x, 0)
            ctx.line_to(x, self.pos.height)
            ctx.stroke()
        # draw nodes
        label_size = 10
        ctx.set_source_rgb(0, 0, 0)
        ctx.set_line_width(1)
        ctx.set_font_size(label_size)
        for dwblrow in self.layout.get_rows(NodeRow):
            for dwbl in dwblrow.get_items(Node):
                ctx.save()
                ctx.translate(*self.pos.center(dwbl.col, dwbl.row))
                self.node_drawer.draw(dwbl.style, dwbl.is_flipped())
                lblrect = ctx.text_extents(dwbl.name)
                ctx.move_to(-lblrect.width / 2, label_size + NODE_SYM_R + 5)
                ctx.show_text(dwbl.name)
                ctx.restore()
        # draw wires
        ctx.set_source_rgb(0, 0, 0)
        ctx.set_line_width(1)
        for dwblrow in self.layout.get_rows():
            for dwbl in dwblrow.get_items(Wire):
                ctx.save()
                x = self.pos.left(dwbl.col[0])
                ctx.translate(x, self.pos.center(dwbl.row))
                self.draw_wire(dwbl, self.pos.right(dwbl.col[1]) - x, *map(width_from_type, dwbl.col))
                ctx.restore()
                self.wire_ends[(dwbl.col[0], dwbl.row)] += 1
                self.wire_ends[(dwbl.col[1], dwbl.row)] += 1
        for dwblcol in self.layout.get_cols(WireColumn):
            for dwbl in dwblcol.get_items(Wire):
                ctx.save()
                y = self.pos.top(dwbl.row[0])
                ctx.translate(self.pos.center(dwbl.col), y)
                ctx.rotate(math.pi / 2)
                self.draw_wire(dwbl, self.pos.bottom(dwbl.row[1]) - y, *map(height_from_type, dwbl.row))
                ctx.restore()
                self.wire_ends[(dwbl.col, dwbl.row[0])] += 1
                self.wire_ends[(dwbl.col, dwbl.row[1])] += 1
        # ... and their dots
        for coord, count in self.wire_ends.items():
            dot_size = 2
            need_dot = count > 2
            if not need_dot and isinstance(coord[0], SupplyColumn):
                need_dot = True
                dot_size = 4
            if not need_dot:
                vertical_wire_on_coord = coord[0].find_item_on_row(coord[1])
                need_dot = count > 2 or \
                           ( isinstance(vertical_wire_on_coord, Wire) and \
                             vertical_wire_on_coord.row.in_range(coord[1]) and \
                             not vertical_wire_on_coord.row.on_endpoint(coord[1]) )
            if need_dot:
                self.ctx.save()
                self.ctx.translate(*self.pos.center(*coord))
                self.draw_dot(dot_size)
                self.ctx.restore()

        # done!
        self._finish()
